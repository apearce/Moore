2017-07-07 Moore v26r5
========================================

Production release for 2017 data taking
----------------------------------------
Based on Gaudi v28r2, LHCb v42r5, Rec v21r5, Phys v23r5, Hlt v26r5

### Bug fixes

- Miscellaneous fixes and improvements, !96 (@rmatev)
  - Fix instantiation of MooreExpert
  - Increase verbosity of apply_conf tests
  - Add a test for the monitoring cache
  - Fix createtck2zeroevents test
- Enable Monitoring for Simulation, !94 (@sstahl)
