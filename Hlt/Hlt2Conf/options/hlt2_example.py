###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_example.py
"""
from __future__ import absolute_import, division, print_function

from PyConf.environment import EverythingHandler, setupInput

from PyConf.Algorithms import FTRawBankDecoder

from Hlt2Conf.setup import setup
from Hlt2Conf.data_from_file import raw_event_from_file
from Hlt2Conf.lines import hlt2_line_builders

from RecoConf.reco_objects_from_file import (
    make_raw_data_for_gec_from_file, upfront_reconstruction,
    stateProvider_with_simplified_geom)
from RecoConf.hlt1_tracking import require_gec

input_files = [
    # MinBias 30000000
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
    # 'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
    # D*-tagged D0 to KK, 27163002
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/27163002/LDST
    'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000012_2.ldst'
]

setup()

ftdec_v = 4
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event
raw_event_format = 4.3

env = EverythingHandler(
    threadPoolSize=1, nEventSlots=1, evtMax=100, debug=False)
setupInput(
    input_files,
    dataType='Upgrade',
    DDDBTag='dddb-20171126',
    CONDDBTag='sim-20171127-vc-md100',
    Simulation=True,
    inputFileType='ROOT')
lines_to_run = [
    'Hlt2CharmHadD02KmPipLine', 'Hlt2CharmHadD02PimPipLine',
    'Hlt2CharmHadD02KmKpLine', 'Hlt2CharmHadDstp2D0Pip_D02KmPipLine'
]

with FTRawBankDecoder.bind(DecodingVersion=ftdec_v), \
     require_gec.bind(make_raw = make_raw_data_for_gec_from_file,
                      FTDecodingVersion=ftdec_v), \
     raw_event_from_file.bind(raw_event_format=raw_event_format):

    for name, builder in hlt2_line_builders(lines_to_run).items():
        env.registerLine(name, upfront_reconstruction() + builder())

env.register_public_tool(stateProvider_with_simplified_geom())

env.configure()
