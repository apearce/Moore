###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Hacks for making legacy and future code work together."""
from __future__ import absolute_import, division, print_function

from Configurables import LoKi__Hybrid__Tool

from PyConf.components import Tool


def patched_hybrid_tool(name):
    """Return a LoKi::Hybrid::Tool configured for non-DVAlgorithms.

    Some modules import functors that depend on the DVAlgorithm context being
    available. The LoKi::Hybrid::Tool tool loads these modules by default,
    breaking algorithms that don't inherit from DVAlgorithm, so we remove them
    from the list.
    """
    # List of modules we will delete from the default list
    dv_modules = ['LoKiPhys.decorators', 'LoKiArrayFunctors.decorators']
    dummy = LoKi__Hybrid__Tool('DummyFactoryNotForUse')

    return Tool(
        LoKi__Hybrid__Tool,
        name='{}HybridFactory'.format(name),
        Modules=[m for m in dummy.Modules if m not in dv_modules])
