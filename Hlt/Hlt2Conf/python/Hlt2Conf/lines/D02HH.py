###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of some D0 -> h- h+ HLT2 lines.

The logic is the same as in Run 1/2: create HLT2 lines by defining selections
which create physics objects. These selections may consume other selections, as
filters or combiners.

The change comes in how selection components are defined.

TODO(AP): More docs.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reco_objects_from_file import make_pvs as get_pvs_from_file

from ..algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs
from ..framework import configurable
from ..standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons
from . import hlt2_line_builder

# Charged pion mass in MeV
_PION_M = 139.57061  # +/- 0.00024


# TODO(AP): implement shared decorator
# @shared
@configurable
def make_selected_particles(make_particles=make_has_rich_long_pions,
                            make_pvs=get_pvs_from_file,
                            trchi2_max=3,
                            mipchi2_min=8,
                            pt_min=800 * MeV,
                            p_min=8 * GeV,
                            pid=None):
    # TODO(AP): this would be a whole lot nicer with f-strings (Python >= 3.6)
    # An alternative is to pass **locals() to format, but... ew
    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


# TODO(AP): bound doesn't exist yet
# make_charm_pions = bound(make_selected_particles, make_has_rich_pions, pid='PIDK < 3')
# make_charm_kaons = bound(make_selected_particles, make_has_rich_pions, pid='PIDK > 3')


@configurable
def make_charm_pions(pid='PIDK < 3'):
    return make_selected_particles(
        make_particles=make_has_rich_long_pions, pid=pid)


@configurable
def make_charm_kaons(pid='PIDK > 3'):
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons, pid=pid)


@configurable
def make_dzeros(particles=None,
                descriptors=None,
                make_pvs=get_pvs_from_file,
                am_min=1715 * MeV,
                am_max=2015 * MeV,
                amaxchild_pt_min=1500 * MeV,
                apt_min=2000 * MeV,
                amindoca_max=0.1 * mm,
                vchi2pdof_max=10,
                bpvvdchi2_min=25,
                acos_bpvdira_min=17.3 * mrad):
    # TODO(AP): TypeError: Cannot decorate a function with positional arguments
    # We want this function to be configurable, but there aren't aren't
    # 'sensible' defaults for these args
    assert particles is not None, 'particles must be specified'
    assert descriptors is not None, 'descriptors must be specified'

    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "AMAXCHILD(PT) > {amaxchild_pt_min}",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       amaxchild_pt_min=amaxchild_pt_min,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    cos_bpvdira_min = math.cos(acos_bpvdira_min)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {cos_bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  cos_bpvdira_min=cos_bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dstars(dzeros=None,
                soft_pions=None,
                descriptors=None,
                make_pvs=get_pvs_from_file,
                q_am_min=130 * MeV - _PION_M,
                q_am_max=165 * MeV - _PION_M,
                q_m_min=130 * MeV - _PION_M,
                q_m_max=160 * MeV - _PION_M,
                vchi2pdof_max=25):
    assert dzeros is not None, 'dzeros must be specified'
    assert soft_pions is not None, 'soft_pions must be specified'
    assert descriptors is not None, 'descriptors must be specified'

    combination_code = require_all(
        "in_range({q_am_min}, (AM - AM1 - AM2), {q_am_max})").format(
            q_am_min=q_am_min,
            q_am_max=q_am_max,
        )

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}",
        "in_range({q_m_min}, (M - M1 - M2), {q_m_max})").format(
            q_m_min=q_m_min, q_m_max=q_m_max, vchi2pdof_max=vchi2pdof_max)

    return ParticleCombinerWithPVs(
        particles=[dzeros, soft_pions],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def charm_prefilters():
    return [require_gec(), require_pvs(get_pvs_from_file())]


@hlt2_line_builder('Hlt2CharmHadD02KmPipLine')
def dzero2Kpi_line():
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    dzeros = make_dzeros(
        particles=[kaons, pions], descriptors=['[D0 -> K- pi+]cc'])
    return charm_prefilters() + [dzeros]


@hlt2_line_builder('Hlt2CharmHadDstp2D0Pip_D02KmPipLine')
def dstar2dzeropi_dzero2Kpi_line():
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    dzeros = make_dzeros(
        particles=[kaons, pions], descriptors=['[D0 -> K- pi+]cc'])
    soft_pions = make_has_rich_long_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'])
    return charm_prefilters() + [dstars]


@hlt2_line_builder('Hlt2CharmHadD02PimPipLine')
def dzero2pipi_line():
    pions = make_charm_pions()
    dzeros = make_dzeros(particles=pions, descriptors=['D0 -> pi- pi+'])
    return charm_prefilters() + [dzeros]


@hlt2_line_builder('Hlt2CharmHadD02KmKpLine')
def dzero2kk_line():
    kaons = make_charm_kaons()
    dzeros = make_dzeros(particles=kaons, descriptors=['D0 -> K- K+'])
    return charm_prefilters() + [dzeros]
