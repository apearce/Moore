###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import re

import wrapt

__all__ = ['hlt2_line_builder', 'hlt2_line_builders']
_hlt2_line_builders = {}
_HLT2_LINE_NAME_PATTERN = re.compile('^Hlt2[A-Za-z0-9_]+Line$')


def _valid_name(name):
    """Return True if name follows the HLT2 line name conventions."""
    return _HLT2_LINE_NAME_PATTERN.match(name) is not None


def hlt2_line_builders(names):
    """Return a dict mapping line names to builder functions.

    Raises KeyError if any name in `names` does not have a builder.
    """
    return {name: _hlt2_line_builders[name] for name in names}


def hlt2_line_builder(name):
    """Decorator to register a named HLT2 line.

    Usage:

        >>> @hlt2_line_builder('Hlt2TheNameOfTheLine')
        ... def the_line_definition():
        ...     # ...
        ...     return [] # filled with control flow
    """
    assert _valid_name(
        name), 'Line name {} is not a valid HLT2 line name'.format(name)

    def wrapper(wrapped):
        # Want to ensure the same name is never assigned to two different
        # functions
        assert name not in _hlt2_line_builders and _hlt2_line_builders.get(
            name,
            None) is not wrapped, '{} already names an HLT2 line: {}'.format(
                name, _hlt2_line_builders[name])
        _hlt2_line_builders[name] = wrapped
        return wrapped

    return wrapper


# Import each module so that the lines are registered
from . import D02HH
from . import Bs2JpsiPhi
