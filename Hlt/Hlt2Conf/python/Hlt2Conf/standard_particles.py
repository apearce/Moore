###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for Particle definitions common across HLT2.

The Run 2 code makes the sensible choice of creating Particle objects first,
and then filtering these with FilterDesktop instances. Because the
FunctionalParticleMaker can apply LoKi cut strings directly to Track and
ProtoParticle objects, we just do the one step.
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad

from PyConf.Algorithms import FunctionalParticleMaker
from PyConf.Tools import (LoKi__Hybrid__ProtoParticleFilter as
                          ProtoParticleFilter, LoKi__Hybrid__TrackSelector as
                          TrackSelector)

from PyConf import configurable

from .algorithms import require_all, ParticleCombiner
from .hacks import patched_hybrid_tool
from RecoConf.reco_objects_from_file import make_protoparticles


@configurable
def long_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrLONG', Code), **kwargs)


@configurable
def down_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrDOWNSTREAM', Code), **kwargs)


@configurable
def upstream_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrUPSTREAM', Code), **kwargs)


@configurable
def ttrack_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrTTRACK', Code), **kwargs)


@configurable
def standard_protoparticle_filter(Code='PP_ALL', **kwargs):
    return ProtoParticleFilter(
        Code=Code, Factory=patched_hybrid_tool('PPFactory'), **kwargs)


@configurable
def make_particles(species='pion',
                   make_protoparticles=make_protoparticles,
                   make_track_selector=long_track_selector,
                   make_protoparticle_filter=standard_protoparticle_filter):
    particles = FunctionalParticleMaker(
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        TrackSelector=make_track_selector(),
        ProtoParticleFilter=make_protoparticle_filter())
    return particles


#Long particles
def make_long_pions():
    return make_particles(
        species="pion",
        make_track_selector=long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_kaons():
    return make_particles(
        species="kaon",
        make_track_selector=long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_protons():
    return make_particles(
        species="proton",
        make_track_selector=long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_muons():
    return make_particles(
        species="muon",
        make_track_selector=long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


#Down particles
def make_down_pions():
    return make_particles(
        species="pion",
        make_track_selector=down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_protons():
    return make_particles(
        species="proton",
        make_track_selector=down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


#T particles
def make_ttrack_pions():
    return make_particles(
        species="pion",
        make_track_selector=ttrack_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_ttrack_protons():
    return make_particles(
        species="proton",
        make_track_selector=ttrack_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def make_phi2kk(am_max=1100. * MeV, adoca_chi2=30, vchi2=25.0):
    kaons = make_kaons()
    descriptors = ['phi(1020) -> K+ K-']
    combination_code = require_all("AM < {am_max}",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_max=am_max, adoca_chi2=adoca_chi2)
    vertex_code = "(VFASPF(VCHI2) < {vchi2})".format(vchi2=vchi2)
    return ParticleCombiner(
        particles=kaons,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_mass_constrained_jpsi2mumu(pid_mu=0,
                                    pt_mu=0.5 * GeV,
                                    admass=150. * MeV,
                                    adoca_chi2=20,
                                    vchi2=16):
    muons = make_muons()
    descriptors = ['J/psi(1S) -> mu+ mu-']
    daughters_code = {
        'mu+':
        '(PIDmu > {pid_mu}) & (PT > {pt_mu})'.format(
            pid_mu=pid_mu, pt_mu=pt_mu)
    }
    combination_code = require_all("ADAMASS('J/psi(1S)') < {admass}",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       admass=admass, adoca_chi2=adoca_chi2)
    vertex_code = require_all("VFASPF(VCHI2) < {vchi2}",
                              "MFIT").format(vchi2=vchi2)
    return ParticleCombiner(
        particles=muons,
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# Make pions
@configurable
def make_has_rich_long_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_pions()


@configurable
def make_has_rich_down_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_pions()


@configurable
def make_has_rich_ttrack_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_ttrack_pions()


# Make kaons
@configurable
def make_has_rich_long_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_kaons()


@configurable
def make_has_rich_down_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_kaons()


@configurable
def make_has_rich_ttrack_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_ttrack_kaons()


# Make protons
@configurable
def make_has_rich_long_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_protons()


@configurable
def make_has_rich_down_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_protons()


@configurable
def make_has_rich_ttrack_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_ttrack_protons()


# Make muons
@configurable
def make_ismuon_long_muon():
    with standard_protoparticle_filter.bind(Code='PP_ISMUON'):
        return make_long_muons()
