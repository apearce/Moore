###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable

from PyConf.Tools import (TrackStateProvider, TrackMasterExtrapolator,
                          SimplifiedMaterialLocator)

from Hlt2Conf.data_from_file import reco_from_file, pid_algorithms, reco_unpackers, make_data_from_file


@configurable
def make_raw_data_for_gec_from_file():
    return make_data_from_file(reco_from_file()['FTCluster'])  #FIXME


# TODO(AP): this from_file should be syncronised with that from `reconstruction`
# mstahl: now that this lives in reco_objects_from_file, i would suggest to remove the from_file boolean plus the ifs and elses. we could also rename make_protoparticles/pvs to get_protoparticles(_from_file)/pvs
@configurable
def upfront_reconstruction(from_file=True):
    '''Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.
    '''
    if from_file:
        handles = list(reco_unpackers().values()) + list(
            pid_algorithms().values())
    else:
        handles = []
    return handles


@configurable
def reconstruction(from_file=True):
    '''Return a dictionary of names to DataHandles that define the reconstruction output.'''
    if from_file:
        handles = {k: v.OutputName for k, v in reco_unpackers().items()}
    else:
        handles = {}

    return handles


def make_protoparticles():
    return reconstruction()['ChargedProtos']


def make_pvs():
    return reconstruction()['PVs']


def stateProvider_with_simplified_geom():
    masterextrapolator = TrackMasterExtrapolator(
        MaterialLocator=SimplifiedMaterialLocator())
    return TrackStateProvider(
        Extrapolator=masterextrapolator, UnsafeClearCache=True)
