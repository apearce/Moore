###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Define the HLT2 track reconstruction outputs for use by lines.'''

from PyConf import configurable

#import makers from Hlt1 config to make our life easier
from RecoConf.hlt1_tracking import (
    make_raw_data,
    make_ft_clusters,
    make_ut_clusters,
    make_velo_tracks,
    make_upstream_tracks,
    make_forward_tracks,
    make_hlt1_tracks,
)

#import already prepared algorithms
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    PrHybridSeeding,
    PrMatchNN,
    PrLongLivedTracking,
    TrackBestTrackCreator,
    VPClus,
    PrStoreFTHit,
    TracksFTConverter,
    #TrackEventFitter,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
)

#import already prepared tools
from PyConf.Tools import (
    PrAddUTHitsTool,
    TrackMasterExtrapolator,
    SimplifiedMaterialLocator,
    DetailedMaterialLocator,
    TrackMasterFitter,
    MeasurementProvider,
    MeasurementProviderT_MeasurementProviderTypes__VP_ as
    VPMeasurementProvider,
    MeasurementProviderT_MeasurementProviderTypes__UTLite_ as
    UTMeasurementProvider,
    FTMeasurementProvider,
)


# Hlt2 specific decoding
@configurable
def make_velo_clusters(make_raw_data=make_raw_data):
    '''Turn raw banks into VP light clusters
    https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrPixel/src/VPClus.{cpp,h}'''
    return VPClus(RawEventLocation=make_raw_data()).ClusterLocation


@configurable
def make_ft_hits(make_ft_clusters=make_ft_clusters):
    '''Write T-station hits to TES taking the detector geometry into account
       https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrAlgorithms/src/PrStoreFTHit.{cpp,h}'''
    return PrStoreFTHit(InputLocation=make_ft_clusters()).FTHitsLocation


# Hlt2 specific makers
## Forward tracking
@configurable
def make_low_pt_forward_tracks(
        input_tracks,
        make_ft_hits=make_ft_hits,
        make_ut_clusters=make_ut_clusters,
):
    '''Hlt2 forward tracking (long tracks from Velo seeds)
       https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrAlgorithms/src/PrForwardTracking.{cpp,h}'''
    uthitstool = PrAddUTHitsTool(UTHitsLocation=make_ut_clusters())

    forward_tracks = PrForwardTrackingVelo(
        FTHitsLocation=make_ft_hits(),
        InputName=input_tracks['Pr'],
        AddUTHitsToolName=uthitstool,
        name='LowPtForwardTracking',
        PropertiesPrint=True,
        UseMomentumEstimate=False,
    ).OutputName

    return forward_tracks


@configurable
def make_scifi_tracks(make_ft_hits=make_ft_hits):
    '''Hybrid seeding
       https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrAlgorithms/src/PrHybridSeeding.{cpp,h},
       https://cds.cern.ch/record/2027531/

       returns a dict of seed tracks, allowing the consumer to choose the container type'''
    scifi_tracks_v2 = PrHybridSeeding(FTHitsLocation=make_ft_hits()).OutputName

    scifi_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=scifi_tracks_v2).OutputTracksName

    return {'v2': scifi_tracks_v2, 'v1': scifi_tracks_v1}


@configurable
def make_match_tracks(velo_tracks,
                      scifi_tracks,
                      make_ut_clusters=make_ut_clusters):
    ''' Long tracks from T-station seeds
        https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrAlgorithms/src/PrMatchNN.{cpp,h}
        returns a dict of match tracks, allowing the consumer to choose the container type '''
    uthitstool = PrAddUTHitsTool(UTHitsLocation=make_ut_clusters())

    match_tracks_v2 = PrMatchNN(
        VeloInput=velo_tracks['v2'],
        SeedInput=scifi_tracks['v2'],
        AddUTHitsToolName=uthitstool).MatchOutput

    match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=match_tracks_v2).OutputTracksName

    return {'v2': match_tracks_v2, 'v1': match_tracks_v1}


@configurable
def make_downstream_tracks(scifi_tracks, make_ut_clusters=make_ut_clusters):
    '''Downstream tracking
       https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrAlgorithms/src/PrLongLivedTracking.{cpp,h}
       returns a dict of downstream tracks, allowing the consumer to choose the container type'''
    downstream_tracks_v2 = PrLongLivedTracking(
        InputLocation=scifi_tracks['v2'],
        UTHits=make_ut_clusters()).OutputLocation

    downstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=downstream_tracks_v2).OutputTracksName

    return {'v2': downstream_tracks_v2, 'v1': downstream_tracks_v1}


@configurable
def get_global_materiallocator(materiallocator=SimplifiedMaterialLocator):
    return materiallocator()


@configurable
def get_global_measurement_provider(
        velo_clusters=make_velo_clusters,
        ut_clusters=make_ut_clusters,
        ft_clusters=make_ft_clusters,
        vp_provider=VPMeasurementProvider,
        ut_provider=UTMeasurementProvider,
        ft_provider=FTMeasurementProvider,
):
    return MeasurementProvider(
        VPProvider=vp_provider(ClusterLocation=velo_clusters()),
        UTProvider=ut_provider(ClusterLocation=ut_clusters()),
        FTProvider=ft_provider(ClusterLocation=ft_clusters()),
    )


## Track fitters
### get a track-fitting tool
@configurable
def get_track_master_fitter(
        get_materiallocator=get_global_materiallocator,
        get_measurement_provider=get_global_measurement_provider):
    '''TrackMasterFitter: https://gitlab.cern.ch/lhcb/Rec/blob/master/Tr/TrackFitter/src/TrackMasterFitter.{cpp,h}'''

    measurement_provider = get_measurement_provider()

    materialLocator = get_materiallocator()

    return TrackMasterFitter(
        MeasProvider=measurement_provider,
        MaterialLocator=materialLocator,
        Extrapolator=TrackMasterExtrapolator(MaterialLocator=materialLocator),
    )


### Event fitter with simplified geometry
#@configurable
#def make_fitted_tracks_simplified_geometry(tracks, fitter_type='TrackMasterFitter'):
#    '''Persist fitted tracks
#       https://gitlab.cern.ch/lhcb/Rec/blob/master/Tr/TrackFitter/src/TrackEventFitter.cpp
#    '''
#    fitter_tool = get_track_fitter_tool(
#        fitter_type=fitter_type, simplifiedGeometry=True)
#    TEFit = TrackEventFitter(TracksInContainer=tracks, Fitter=fitter_tool)
#    return TEFit.TracksOutContainer
#
#### Event fitter with full geometry
#@configurable
#def make_fitted_tracks_full_geometry(tracks, fitter_type='TrackMasterFitter'):
#    '''Persist fitted tracks
#       https://gitlab.cern.ch/lhcb/Rec/blob/master/Tr/TrackFitter/src/TrackEventFitter.cpp
#    '''
#    fitter_tool = get_track_fitter_tool(
#        fitter_type=fitter_type, simplifiedGeometry=False)
#    TEFit = TrackEventFitter(TracksInContainer=tracks, Fitter=fitter_tool)
#    return TEFit.TracksOutContainer


## fill the best track container
@configurable
def make_best_tracks(velo_tracks,
                     upstream_tracks,
                     hlt2_forward_tracks,
                     hlt1_forward_tracks,
                     scifi_tracks,
                     downstream_tracks,
                     match_tracks,
                     get_fitter_tool=get_track_master_fitter):
    '''Persist best quality tracks, call track fitters and kill clones on the way
       https://gitlab.cern.ch/lhcb/Rec/blob/master/Tr/TrackUtils/src/TrackBestTrackCreator.{cpp,h}'''
    #TODO: the list has a random order, pls fix
    tracklists = [
        velo_tracks, hlt2_forward_tracks, hlt1_forward_tracks, upstream_tracks,
        downstream_tracks, match_tracks, scifi_tracks
    ]

    fitter_tool = get_fitter_tool()
    bestTrackCreator = TrackBestTrackCreator(
        TracksInContainers=tracklists,
        InitTrackStates=False,
        DoNotRefit=True,
        Fitter=fitter_tool,
        FitTracks=True,
    )
    return bestTrackCreator.TracksOutContainer


def make_hlt2_tracks():

    hlt1_tracks = make_hlt1_tracks()
    hlt2_forward_tracks = make_forward_tracks(
        hlt1_tracks["Velo"], make_forward_tracks_pr=make_low_pt_forward_tracks)
    scifi_tracks = make_scifi_tracks()
    downstream_tracks = make_downstream_tracks(scifi_tracks)
    match_tracks = make_match_tracks(hlt1_tracks["Velo"], scifi_tracks)

    track_version = 'v1'
    best_tracks = make_best_tracks(
        velo_tracks=hlt1_tracks["Velo"][track_version],
        upstream_tracks=hlt1_tracks["Upstream"][track_version],
        hlt2_forward_tracks=hlt2_forward_tracks[track_version],
        hlt1_forward_tracks=hlt1_tracks["Forward"]
        [track_version],  #TODO fitted or not?
        scifi_tracks=scifi_tracks[track_version],
        downstream_tracks=downstream_tracks[track_version],
        match_tracks=match_tracks[track_version])
    return {
        "Velo": hlt1_tracks["Velo"],
        "Upstream": hlt1_tracks["Upstream"],
        "ForwardFast": hlt1_tracks["Forward"],
        "Forward": hlt2_forward_tracks,
        "Seed": scifi_tracks,
        "Downstream": downstream_tracks,
        "Match": match_tracks,
        "Best": best_tracks,
    }
