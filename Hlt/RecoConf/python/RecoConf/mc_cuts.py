###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import mm, GeV
MCCuts = {
    "Velo": {
        "01_velo": "isVelo",
        "02_long": "isLong",
        "03_long>5GeV": "isLong & over5",
        "04_long_strange": "isLong & strange",
        "05_long_strange>5GeV": "isLong & strange & over5",
        "06_long_fromB": "isLong & fromB",
        "07_long_fromB>5GeV": "isLong & fromB & over5",
        "08_long_electrons": "isLong & isElectron",
        "09_long_fromB_electrons": "isLong & isElectron & fromB",
        "10_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "Forward": {
        "01_long": "isLong",
        "02_long>5GeV": "isLong & over5",
        "03_long_strange": "isLong & strange",
        "04_long_strange>5GeV": "isLong & strange & over5",
        "05_long_fromB": "isLong & fromB",
        "06_long_fromB>5GeV": "isLong & fromB & over5",
        "07_long_electrons": "isLong & isElectron",
        "08_long_fromB_electrons": "isLong & isElectron & fromB",
        "09_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "Up": {
        "01_velo": "isVelo",
        "02_velo+UT": "isVelo & isUT",
        "03_velo+UT>5GeV": "isVelo & isUT & over5",
        "04_velo+notLong": "isNotLong & isVelo ",
        "05_velo+UT+notLong": "isNotLong & isVelo & isUT",
        "06_velo+UT+notLong>5GeV": "isNotLong & isVelo & isUT & over5",
        "07_long": "isLong",
        "08_long>5GeV": "isLong & over5 ",
        "09_long_fromB": "isLong & fromB",
        "10_long_fromB>5GeV": "isLong & fromB & over5",
        "11_long_electrons": "isLong & isElectron",
        "12_long_fromB_electrons": "isLong & isElectron & fromB",
        "13_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "T": {
        "01_hasT":
        "isSeed ",
        "02_long":
        "isLong",
        "03_long>5GeV":
        "isLong & over5",
        "04_long_fromB":
        "isLong & fromB",
        "05_long_fromB>5GeV":
        "isLong & fromB & over5",
        "06_UT+T_strange":
        "strange & isDown",
        "07_UT+T_strange>5GeV":
        "strange & isDown & over5",
        "08_noVelo+UT+T_strange":
        "strange & isDown & isNotVelo",
        "09_noVelo+UT+T_strange>5GeV":
        "strange & isDown & over5 & isNotVelo",
        "10_UT+T_SfromDB":
        "strange & isDown & ( fromB | fromD )",
        "11_UT+T_SfromDB>5GeV":
        "strange & isDown & over5 & ( fromB | fromD )",
        "12_noVelo+UT+T_SfromDB>5GeV":
        "strange & isDown & isNotVelo & over5 & ( fromB | fromD )"
    },
    "Down": {
        "01_UT+T":
        "isDown ",
        "02_UT+T>5GeV":
        "isDown & over5",
        "03_UT+T_strange":
        " strange & isDown",
        "04_UT+T_strange>5GeV":
        " strange & isDown & over5",
        "05_noVelo+UT+T_strange":
        " strange & isDown & isNotVelo",
        "06_noVelo+UT+T_strange>5GeV":
        " strange & isDown & over5 & isNotVelo",
        "07_UT+T_fromB":
        "isDown & fromB",
        "08_UT+T_fromB>5GeV":
        "isDown & fromB & over5",
        "09_noVelo+UT+T_fromB":
        "isDown & fromB & isNotVelo",
        "10_noVelo+UT+T_fromB>5GeV":
        "isDown & fromB & over5 & isNotVelo",
        "11_UT+T_SfromDB":
        " strange & isDown & ( fromB | fromD )",
        "12_UT+T_SfromDB>5GeV":
        " strange & isDown & over5 & ( fromB | fromD )",
        "13_noVelo+UT+T_SfromDB":
        " strange & isDown & isNotVelo & ( fromB | fromD )",
        "14_noVelo+UT+T_SfromDB>5GeV":
        " strange & isDown & isNotVelo & over5 & ( fromB | fromD ) "
    },
    "UTForward": {
        "01_long": "isLong",
        "02_long>5GeV": "isLong & over5"
    },
    "UTDown": {
        "01_has seed": "isSeed",
        "02_has seed +noVelo, T+UT": "isSeed & isNotVelo & isDown",
        "03_down+strange": "strange & isDown",
        "04_down+strange+>5GeV": "strange & isDown & over5",
        "05_pi<-Ks<-B": "fromKsFromB",
        "06_pi<-Ks<-B+> 5 GeV": "fromKsFromB & over5"
    },
}
TriggerMCCuts = {
    "Velo": {
        "11_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "12_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "Forward": {
        "10_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "11_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "Up": {
        "14_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "15_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "New": {
        "long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTForward": {
        "03_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "04_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTDown": {
        "07_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "08_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTNew": {
        "long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
}


def get_mc_cuts(key, triggerNumbers=False):
    cuts = MCCuts[key] if key in MCCuts else {}
    if triggerNumbers and key in TriggerMCCuts:
        cuts.update(TriggerMCCuts[key])
    return cuts
