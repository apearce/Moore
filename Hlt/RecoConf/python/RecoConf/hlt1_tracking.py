###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.components import (
    make_algorithm,
    Algorithm,
    force_location,
    setup_component,
)

from Configurables import (
    Gaudi__Hive__FetchDataFromFile,
    PrGECFilter,
    PrStoreUTHit,
    VoidFilter,
    PrVeloUT,
    ParameterizedKalmanFit,
)

# all trivial (not changing default) and fully datahandly imports can be done from here
from PyConf.Algorithms import (
    MakeSelection__Track_v1 as TrackV1Selection,
    MakeSelection__Track_v2 as TrackV2Selection,
    LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as
    FromV2TrackV1TrackVector,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
    FTRawBankDecoder,
    TracksVPMergerConverter,
    TracksVPConverter,
    TracksUTConverter,
    TracksFTConverter,
    TracksFitConverter,
    VeloClusterTrackingSIMD,
    SciFiTrackForwarding,
    TrackBeamLineVertexFinderSoA,
    SciFiTrackForwardingStoreHit,
    VeloKalman,
    createODIN,
    LHCb__MDF__IOAlg,
)

from GaudiKernel.SystemOfUnits import mm, MeV


def __rawdata_transform_outputs(RawEvent):
    return {"DataKeys": [RawEvent]}


RawData = make_algorithm(
    Gaudi__Hive__FetchDataFromFile,
    outputs={'RawEvent': force_location('/Event/DAQ/RawEvent')},
    output_transform=__rawdata_transform_outputs,
    require_IOVLock=False)

UTDecoding = make_algorithm(
    PrStoreUTHit, defaults=dict(skipBanksWithErrors=True))

VeloUTTracking = make_algorithm(PrVeloUT, defaults=dict(minPT=300 * MeV))


def __emptyfilter_input_transform(InputLocation):
    from Functors import SIZE
    fun = SIZE(InputLocation) > 0
    return {"Code": fun.code(), "Headers": fun.headers()}


EmptyFilter = make_algorithm(
    VoidFilter, input_transform=__emptyfilter_input_transform)


def make_RawData():
    return RawData().RawEvent


def make_RawData_IOSvc(inputFiles):
    setup_component('LHCb__MDF__IOSvcMM', Input=inputFiles)
    return LHCb__MDF__IOAlg(
        IOSvc="LHCb::MDF::IOSvcMM/LHCb__MDF__IOSvcMM").RawEventLocation


@configurable
def make_raw_data(make_raw=make_RawData):
    return make_raw()


@configurable
def make_odin(make_raw_data=make_raw_data):
    return createODIN(RawEvent=make_raw_data()).ODIN


@configurable
def require_gec(make_raw=make_raw_data, FTDecodingVersion=None, **kwargs):
    # TODO use FTDecodingVersion=smart.REQUIRED above
    cut = 9750 if FTDecodingVersion == 4 else 11000
    props = dict(NumberFTUTClusters=cut, **kwargs)  # kwargs take precedence
    return Algorithm(PrGECFilter, RawEventLocations=make_raw(), **props)


## VELO
@configurable
def make_velo_hits(make_raw=make_raw_data):
    return VeloClusterTrackingSIMD(RawEventLocation=make_raw()).HitsLocation


@configurable
def make_velo_tracks_simdly(make_raw=make_raw_data):
    tracking = VeloClusterTrackingSIMD(RawEventLocation=make_raw())
    return {
        "Forward": tracking.TracksLocation,
        "Backward": tracking.TracksBackwardLocation
    }


@configurable
def make_velo_tracks(make_velo_pr_tracks=make_velo_tracks_simdly,
                     make_velo_hits=make_velo_hits):
    ''' returns a dict of velo tracks, allowing the consumer to choose the container type'''
    velo_tracks = make_velo_pr_tracks()
    velo_pr_tracks = velo_tracks['Forward']
    velo_pr_tracks_backward = velo_tracks['Backward']

    velo_tracks_v2 = TracksVPMergerConverter(
        TracksForwardLocation=velo_pr_tracks,
        TracksBackwardLocation=velo_pr_tracks_backward,
        HitsLocation=make_velo_hits()).OutputTracksLocation

    velo_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=velo_tracks_v2).OutputTracksName

    return {
        "Pr": velo_pr_tracks,
        "Pr::backward": velo_pr_tracks_backward,
        "v2": velo_tracks_v2,
        "v1": velo_tracks_v1
    }


@configurable
def make_pvs_tblv(velo_tracks):
    return TrackBeamLineVertexFinderSoA(
        TracksLocation=velo_tracks["Pr"],
        TracksBackwardLocation=velo_tracks["Pr::backward"]).OutputVertices


@configurable
def make_pvs(make_velo_tracks=make_velo_tracks, make_pvs=make_pvs_tblv):
    return make_pvs(make_velo_tracks())


@configurable
def require_pvs(pvs):
    return EmptyFilter(name='require_pvs', InputLocation=pvs)


## UPSTREAM
@configurable
def make_ut_clusters(make_raw=make_raw_data):
    return UTDecoding(RawEventLocations=make_raw()).UTHitsLocation


@configurable
def make_velout_tracks(velo_tracks, make_ut_clusters=make_ut_clusters):
    return VeloUTTracking(
        InputTracksName=velo_tracks["Pr"],
        UTHits=make_ut_clusters()).OutputTracksName


@configurable
def make_upstream_tracks(velo_tracks,
                         make_upstream_tracks_pr=make_velout_tracks):
    ''' returns a dict of upstream tracks, allowing the consumer to choose the container type'''
    upstream_tracks_pr = make_upstream_tracks_pr(velo_tracks=velo_tracks)

    upstream_tracks_v2 = TracksUTConverter(
        TracksVPLocation=velo_tracks["v2"],
        TracksUTLocation=upstream_tracks_pr).OutputTracksLocation

    upstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=upstream_tracks_v2).OutputTracksName

    return {
        "Pr": upstream_tracks_pr,
        "v2": upstream_tracks_v2,
        "v1": upstream_tracks_v1
    }


## FORWARD
@configurable
def make_ft_clusters(make_raw=make_raw_data):
    return FTRawBankDecoder(RawEventLocations=make_raw()).OutputLocation


@configurable
def make_ft_hits(make_ft_clusters=make_ft_clusters):
    return SciFiTrackForwardingStoreHit(HitsLocation=make_ft_clusters()).Output


@configurable
def make_tracks_with_scifitrackforwarding(input_tracks,
                                          make_ft_hits=make_ft_hits):
    return SciFiTrackForwarding(
        HitsLocation=make_ft_hits(), InputTracks=input_tracks["Pr"]).Output


@configurable
def make_forward_tracks(
        input_tracks,
        make_forward_tracks_pr=make_tracks_with_scifitrackforwarding):
    ''' returns a dict of forward tracks, allowing the consumer to choose the container type'''
    forward_tracks_pr = make_forward_tracks_pr(input_tracks=input_tracks)
    forward_tracks_v2 = TracksFTConverter(
        TracksUTLocation=input_tracks["v2"],
        TracksFTLocation=forward_tracks_pr).OutputTracksLocation
    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName
    return {
        "Pr": forward_tracks_pr,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1
    }


@configurable
def make_velokalman_fitted_tracks(tracks, make_hits=make_velo_hits):
    ''' returns a dict of forward fitted tracks, allowing the consumer to choose the container type'''
    fitted_tracks = VeloKalman(
        HitsLocation=make_hits(),
        TracksVPLocation=tracks["Velo"]["Pr"],
        TracksFTLocation=tracks["Forward"]["Pr"]).OutputTracksLocation

    fitted_tracks_v2 = TracksFitConverter(
        TracksFTLocation=tracks["Forward"]["v2"],
        TracksFitLocation=fitted_tracks).OutputTracksLocation
    fitted_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=fitted_tracks_v2).OutputTracksName
    fitted_tracks_v1_sel = TrackV1Selection(
        Input=FromV2TrackV1TrackVector(
            InputTracksName=fitted_tracks_v2).OutputTracksName)
    fitted_tracks_v2_sel = TrackV2Selection(Input=fitted_tracks_v2).Output
    return {
        "Pr": fitted_tracks,
        "v2": fitted_tracks_v2,
        "v1": fitted_tracks_v1,
        "v1Sel": fitted_tracks_v1_sel,
        "v2Sel": fitted_tracks_v2_sel,
    }


def make_hlt1_tracks():
    velo_tracks = make_velo_tracks()
    velo_ut_tracks = make_upstream_tracks(velo_tracks)
    forward_tracks = make_forward_tracks(velo_ut_tracks)
    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "Forward": forward_tracks,
    }
