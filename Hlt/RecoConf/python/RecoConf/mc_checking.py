###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
'''
In this file, algorithms needed to run the MC reconstruction checking (PrTrackChecker) are defined.
'''
from PyConf.tonic import (configurable)

from PyConf.components import (Algorithm, force_location, Tool)

from PyConf.Algorithms import (
    VPClusFull,
    VPFullCluster2MCParticleLinker,
    PrLHCbID2MCParticle,
    PrTrackAssociator,
    PrTrackChecker,
)

from RecoConf.hlt1_tracking import (
    make_raw_data,
    make_ut_clusters,
    make_ft_clusters,
)

from Hlt2Conf.data_from_file import (mc_unpackers, make_data_from_file)

from RecoConf.mc_cuts import get_mc_cuts


@configurable
def make_velo_full_clusters(make_raw=make_raw_data):
    return VPClusFull(RawEventLocation=make_raw()).ClusterLocation


@configurable
def make_links_veloclusters_mcparticles():
    return VPFullCluster2MCParticleLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCParticleLinksLocation=make_data_from_file(
            "/Event/Link/Raw/VP/Digits")).OutputLocation


@configurable
def make_links_lhcbids_mcparticles():
    return PrLHCbID2MCParticle(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_ut_clusters(),
        UTHitsLinkLocation=make_data_from_file('/Event/Link/Raw/UT/Clusters'),
        FTLiteClustersLocation=make_ft_clusters(),
        FTLiteClustersLinkLocation=make_data_from_file(
            '/Event/Link/Raw/FT/LiteClusters')).TargetName


@configurable
def make_links_tracks_mcparticles(
        InputTracks, LinkerLocationID=make_links_lhcbids_mcparticles):
    return PrTrackAssociator(
        SingleContainer=InputTracks["v1"],
        LinkerLocationID=LinkerLocationID(),
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation


@configurable
def monitor_track_efficiency(
        InputTracks,
        TrackType="Forward",
        Links=make_links_tracks_mcparticles,
):
    props = dict(
        Title=TrackType,
        HitTypesToCheck=8,
        WriteHistos=1,
        MyCuts=get_mc_cuts(TrackType))
    from Configurables import LoKi__Hybrid__MCTool
    myFactory = Tool(LoKi__Hybrid__MCTool, Modules=["LoKiMC.decorators"])

    return PrTrackChecker(
        Tracks=InputTracks["v1"],
        Links=Links(InputTracks),
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=make_links_lhcbids_mcparticles(),
        MCPropertyInput=make_data_from_file("/Event/MC/TrackInfo"),
        LoKiFactory=myFactory,
        **props)
