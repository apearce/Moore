###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from RecoConf.hlt1_tracking import (require_gec, require_pvs, make_pvs,
                                    make_hlt1_tracks,
                                    make_velokalman_fitted_tracks)

from PyConf.Algorithms import FTRawBankDecoder
from PyConf.environment import EverythingHandler, setupInputFromTestFileDB

ftdec_v = 4


def hlt1_full_track_reco_line():
    with FTRawBankDecoder.bind(DecodingVersion=ftdec_v):
        gec = require_gec(FTDecodingVersion=ftdec_v)
        NoPVFilter = require_pvs(make_pvs())
        fitted_tracks = make_velokalman_fitted_tracks(make_hlt1_tracks())["Pr"]

    return [gec, NoPVFilter, fitted_tracks]


env = EverythingHandler(
    threadPoolSize=1, nEventSlots=1, evtMax=1000, debug=True)
env.registerLine('Hlt1_reco_baseline', algs=hlt1_full_track_reco_line())
setupInputFromTestFileDB('MiniBrunel_2018_MinBias_FTv4_DIGI')
env.configure()
