###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
    make_hlt1_tracks,
)
from PyConf.Algorithms import (FTRawBankDecoder)
from RecoConf.mc_checking import (monitor_track_efficiency)
from PyConf.environment import (EverythingHandler, setupInputFromTestFileDB)

ftdec_v = 4


def mc_matching_line():
    with FTRawBankDecoder.bind(DecodingVersion=ftdec_v):
        gec = require_gec(FTDecodingVersion=ftdec_v)
        NoPVFilter = require_pvs(make_pvs())
        Hlt1_forward_efficiencies = monitor_track_efficiency(
            make_hlt1_tracks()["Forward"])
    return [gec, NoPVFilter, Hlt1_forward_efficiencies]


env = EverythingHandler(
    threadPoolSize=1,
    nEventSlots=1,
    evtMax=1000,
    debug=True,
    HistoFile="MCMatching_Hlt1ForwardTracking.root")
env.registerLine('MCMatching_Hlt1ForwardTracking', algs=mc_matching_line())
setupInputFromTestFileDB('MiniBrunel_2018_MinBias_FTv4_DIGI')
env.configure()
