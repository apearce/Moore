###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.environment import EverythingHandler, setupInputFromTestFileDB
from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
)
from RecoConf.hlt2_tracking import make_hlt2_tracks, get_global_materiallocator
from PyConf.Algorithms import FTRawBankDecoder
from PyConf.Tools import DetailedMaterialLocator

ftdec_v = 4


def hlt2_full_geometry_track_reco_line():
    with FTRawBankDecoder.bind(DecodingVersion=ftdec_v), \
         get_global_materiallocator.bind(materiallocator = DetailedMaterialLocator):
        gec = require_gec(FTDecodingVersion=ftdec_v)
        NoPVFilter = require_pvs(make_pvs())
        best_tracks = make_hlt2_tracks()["Best"]
    return [gec, NoPVFilter, best_tracks]


env = EverythingHandler(
    threadPoolSize=1, nEventSlots=1, evtMax=100, debug=True)
env.registerLine(
    "Hlt2_reco_full_geometry", algs=hlt2_full_geometry_track_reco_line())
setupInputFromTestFileDB('MiniBrunel_2018_MinBias_FTv4_DIGI')
env.configure()
