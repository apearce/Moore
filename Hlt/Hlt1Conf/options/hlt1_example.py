###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.environment import EverythingHandler, setupInputFromTestFileDB
from PyConf.Algorithms import FTRawBankDecoder
from RecoConf.hlt1_tracking import require_gec
from Hlt1Conf.lines.track_mva import (one_track_mva_line, two_track_mva_line,
                                      debug_two_track_mva_line)

ftdec_v = 4
env = EverythingHandler(
    threadPoolSize=1, nEventSlots=1, evtMax=1000, debug=True)

with FTRawBankDecoder.bind(DecodingVersion=ftdec_v), \
     require_gec.bind(FTDecodingVersion=ftdec_v):

    builders = {
        'Hlt1TrackMVALine': one_track_mva_line,
        'Hlt1TwoTrackMVALine': two_track_mva_line,
        'Hlt1DebugTwoTrackMVALine': debug_two_track_mva_line,
    }
    for name, builder in builders.items():
        env.registerLine(name, builder())

setupInputFromTestFileDB('MiniBrunel_2018_MinBias_FTv4_DIGI')
env.configure()
