###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run HLT1 configured for throughput tests.

The differences with respect to hlt1_example.py are:

* Assume the input files are available on a RAM disk `/run/swtest`, which
  avoids the penalty of loading each file from disk.
* Assumes the existance of a file 00067189.mdf on that RAM disk.
* Uses the EvtStoreSvc to handle interactions with the TES, as this is faster
  than the default HiveWhiteBoard (although doesn't work on DSTs).
* Uses an alternative, faster IOSvc.
* Removes the Hlt1DebugTwoTrackMVALine/debug_two_track_mva_line line, which has
  unrealistically loose cuts and overlead from ROOT TTree I/O and should not,
  therefore, be included in throughput tests.

The latter two points may be made defaults for all HLT1 execution in the
future, but are not currently compatible with certain input types.
"""
from PyConf.environment import EverythingHandler, setupInputFromTestFileDB
from PyConf.Algorithms import FTRawBankDecoder
from RecoConf.hlt1_tracking import (require_gec, make_raw_data,
                                    make_RawData_IOSvc)
from Hlt1Conf.lines.track_mva import (one_track_mva_line, two_track_mva_line)

RAM_DISK = '/run/swtest'
INPUT_FILE = '00067189.mdf'

tes = 'EvtStoreSvc'
env = EverythingHandler(
    threadPoolSize=1, nEventSlots=1, evtMax=100000, debug=True, TESName=tes)

ftdec_v = 4
inputFiles = setupInputFromTestFileDB(
    'MiniBrunel_2018_MinBias_FTv4_MDF',
    # Run over the single file 50 times to make sure we process lots of events
    inputFiles=[os.path.join(RAM_DISK, INPUT_FILE)] * 50,
    TESName=tes,
    fileType='MDF',
    withEventSelector=False)
make_input_data = lambda: make_RawData_IOSvc(inputFiles)
with FTRawBankDecoder.bind(DecodingVersion=ftdec_v), \
     require_gec.bind(FTDecodingVersion=ftdec_v), \
     make_raw_data.bind(make_raw=make_input_data):
    builders = {
        'Hlt1TrackMVALine': one_track_mva_line,
        'Hlt1TwoTrackMVALine': two_track_mva_line,
    }
    for name, builder in builders.items():
        env.registerLine(name, builder())

env.configure()
# env.plotDataFlow()
