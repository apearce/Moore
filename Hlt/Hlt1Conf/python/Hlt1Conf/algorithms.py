###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
from PyConf import configurable
from Configurables import (
    PrFilter__Track_v1,
    PrFilter__Track_v2,
    PrFilter__PrFittedForwardTracks,
    TrackCombiner,
    CombineTracks__2Body__Track_v2,
    CombineTracks__2Body__Track_v2__Dumper,
    CombineTracks__2Body__PrFittedForwardTracks,
)

from PyConf.components import Algorithm


def require_all(*cuts):
    """Return a cut string that requires each of the argument strings.

    Example usage:

        >>> require_all('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) & (PT > 3*GeV))'
    """
    return "({})".format(" & ".join("({})".format(cut) for cut in cuts))


def require_any(*cuts):
    """Return a cut string that requires at least one of the argument strings.

    Example usage:

        >>> require_any('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) | (PT > 3*GeV))'
    """
    return "({})".format(" | ".join("({})".format(cut) for cut in cuts))


@configurable  # cant make this a make_algorithm because the input transform function neeeds the kwargs
def TrackV1FilterWithPVs(Preambulo=[], **kwargs):
    def __transform_inputs(InputVertices, Input):
        return {
            "Preambulo": [  # TODO define special case for this preambulo shit
                # Redefine the functor to include the PV location
                "TrMINIPCHI2 = TrMINIPCHI2('{}')".format(InputVertices)
            ] + Preambulo,
            "Input":
            Input
        }

    return Algorithm(
        PrFilter__Track_v1, input_transform=__transform_inputs, **kwargs)


@configurable
def TrackCombinerWithPVs(Preamble=[], **kwargs):
    def transform_inputs(InputVertices, InputTracks):
        return {
            "Preamble": [
                # Redefine the functor to include the PV location
                "TrMINIPCHI2 = TrMINIPCHI2('{}')".format(InputVertices)
            ] + Preamble,
            "InputTracks":
            InputTracks
        }

    return Algorithm(TrackCombiner, input_transform=transform_inputs, **kwargs)


@configurable
def PrFilterV2Tracks(functor, **kwargs):
    return Algorithm(
        PrFilter__Track_v2,
        Code=functor.code(),
        Headers=functor.headers(),
        Factory='FunctorFactory',
        **kwargs)


@configurable
def FilterPrFittedForwardTracks(functor, **kwargs):
    return Algorithm(
        PrFilter__PrFittedForwardTracks,
        Code=functor.code(),
        Headers=functor.headers(),
        Factory='FunctorFactory',
        **kwargs)


@configurable
def CombineTracks(NBodies=2,
                  CombinationCut=None,
                  VertexCut=None,
                  VoidDump=None,
                  ChildDump=None,
                  CombinationDump=None,
                  VertexDump=None,
                  PrTracks=False,
                  **kwargs):
    """Return a configured CombineTracks instance.

    If any of `ChildDump`, `CombinationDump`, or `VertexDump` are not None, a
    version of the combiner is returned that will produce an ntuple. The ntuple
    has one row for each combination, and is useful for inspecting values that
    the combiner uses to evaluate selections. Each `Dump` argument is a
    dictionary whose keys are strings, corresponding to the row in the ntuple
    that will be filled with the functor value.

    Parameters
    ----------
    NBodies : int
        The number of tracks entering each combination. Currently only 2-track
        combinations are supported.
    CombinationCut : Functors.Functor
        Functor to be applied to the N-track combination object.
    VertexCut : Functors.Functor
        Functor to be applied to the vertex object.
    VoidDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    ChildDump : dict of str to Functors.Functor
        Functors to be evaluated for each child in each combination when
        creating an ntuple.
    CombinationDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    VertexDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    PrTracks : bool
        If True, a combiner algorithm instance that accepts
        PrFittedForwardTrack objects is returned. Otherwise, an instance that
        accepts Track::v2 objects is returned.
    """
    assert NBodies == 2

    def parse(input_dict):
        if input_dict is None: return {}
        return {k: [v.code()] + v.headers() for k, v in input_dict.items()}

    VoidDump_dict = parse(VoidDump)
    ChildDump_dict = parse(ChildDump)
    CombinationDump_dict = parse(CombinationDump)
    VertexDump_dict = parse(VertexDump)
    enable_dumper = len(ChildDump_dict) or len(CombinationDump_dict) or len(
        VertexDump_dict)
    if PrTracks:
        AlgType = CombineTracks__2Body__PrFittedForwardTracks
    elif enable_dumper:
        AlgType = CombineTracks__2Body__Track_v2__Dumper
    else:
        AlgType = CombineTracks__2Body__Track_v2
    return Algorithm(
        AlgType,
        VertexCut_Code=VertexCut.code(),
        VertexCut_Headers=VertexCut.headers(),
        CombinationCut_Code=CombinationCut.code(),
        CombinationCut_Headers=CombinationCut.headers(),
        VoidDump_Functors=VoidDump_dict,
        ChildDump_Functors=ChildDump_dict,
        CombinationDump_Functors=CombinationDump_dict,
        VertexDump_Functors=VertexDump_dict,
        **kwargs)
