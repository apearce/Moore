###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import math
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from RecoConf.hlt1_tracking import (require_gec, require_pvs, make_pvs,
                                    make_odin, make_hlt1_tracks,
                                    make_velokalman_fitted_tracks)
from ..algorithms import (CombineTracks, PrFilterV2Tracks,
                          FilterPrFittedForwardTracks)
from PyConf.Algorithms import (FTRawBankDecoder, MakeIterableTracks)


def make_tracks_mva_tracks():
    return make_velokalman_fitted_tracks(make_hlt1_tracks())


@configurable
def track_mva_prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def one_track_mva_line(
        make_input_tracks=make_tracks_mva_tracks,
        make_pvs=make_pvs,
        # TrackMVALoose cuts from ZombieMoore
        max_chi2dof=2.5,
        min_pt=2.0 * GeV,
        max_pt=26 * GeV,
        min_ipchi2=7.4,
        param1=1.0,
        param2=2.0,
        param3=1.248):
    pvs = make_pvs().location
    from Functors import PT, CHI2DOF, MINIPCHI2, MINIPCHI2CUT
    from Functors.math import in_range, log
    pre_sel = (CHI2DOF < max_chi2dof)
    hard_sel = (PT > max_pt) & MINIPCHI2CUT(IPChi2Cut=min_ipchi2, Vertices=pvs)
    bulk_sel = in_range(min_pt, PT, max_pt) & (
        log(MINIPCHI2(pvs)) >
        (param1 / ((PT / GeV - param2) * (PT / GeV - param2)) +
         (param3 / max_pt) * (max_pt - PT) + math.log(min_ipchi2)))
    full_sel = pre_sel & (hard_sel | bulk_sel)
    track_filter = FilterPrFittedForwardTracks(
        full_sel, Input=make_input_tracks()['Pr'])
    return track_mva_prefilters() + [track_filter]


@configurable
def two_track_mva_line(make_input_tracks=make_tracks_mva_tracks,
                       make_pvs=make_pvs):
    # This corresponds to TwoTrackMVALoose in ZombieMoore
    from Functors.math import in_range
    from Functors import (P, PT, CHI2DOF, DOCACHI2, BPVDIRA, MINIPCHI2CUT, MVA,
                          COMB, SUM, BPVIPCHI2, BPVFDCHI2, BPVETA, BPVCORRM)
    from GaudiKernel.SystemOfUnits import MeV, GeV
    pvs = make_pvs().location
    ChildCut = ((PT > 800. * MeV) & (P > 5. * GeV) &
                (CHI2DOF < 2.5) & MINIPCHI2CUT(IPChi2Cut=4., Vertices=pvs))
    pr_children = FilterPrFittedForwardTracks(
        ChildCut, Input=make_input_tracks()['Pr']).Output
    children = MakeIterableTracks(Input=pr_children).Output
    CombinationCut = (PT > 2. * GeV) & (DOCACHI2 < 10.)

    def apply_to_combination(functor):
        """Helper for applying 'Combination' cuts to a composite."""
        return COMB(
            Functor=functor,
            ChildContainers=[children.location],
            ChildContainerTypes=[
                ('LHCb::Pr::Iterable::Fitted::Forward::Tracks',
                 'Event/PrIterableTracks.h')
            ])

    # TODO there seems to be some magic that substitutes the value of
    #      $PARAMFILESROOT before it gets to the C++. This is probably not what
    #      we want -- better to resolve it at runtime than bake a value into
    #      the functor cache, for example
    VertexCut = ((CHI2DOF < 10.) & (BPVDIRA(pvs) > 0) & (
        BPVCORRM(Mass=139.57018 * MeV, Vertices=pvs) > 1. * GeV
    ) & in_range(2., BPVETA(pvs), 5.) & (MVA(
        MVAType='MatrixNet',
        Config={'MatrixnetFile': "${PARAMFILESROOT}/data/Hlt1TwoTrackMVA.mx"},
        Inputs={
            'chi2': CHI2DOF,
            'fdchi2': BPVFDCHI2(pvs),
            'sumpt': apply_to_combination(SUM(PT)),
            'nlt16': apply_to_combination(SUM(BPVIPCHI2(pvs) < 16.)),
        }) > 0.96))

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        VertexCut=VertexCut,
        InputTracks=children,
        CombinationCut=CombinationCut)
    return track_mva_prefilters() + [combination_filter]


@configurable
def debug_two_track_mva_line(make_input_tracks=make_tracks_mva_tracks,
                             make_pvs=make_pvs,
                             VertexCut=None,
                             ChildCut=None,
                             CombinationCut=None,
                             VoidDump=None,
                             ChildDump=None,
                             CombinationDump=None,
                             VertexDump=None,
                             dump_filename='PrCombineTracks.root'):

    from Functors import (ALL, ETA, P, PT, CHI2DOF, SUM, DOCA, DOCACHI2,
                          BPVDIRA, BPVIPCHI2, MINIP, PHI, RUNNUMBER,
                          EVENTNUMBER, EVENTTYPE, BPVIPCHI2STATE, COMB)
    from GaudiKernel.SystemOfUnits import MeV
    pv_loc, odin_loc = make_pvs().location, make_odin().location
    if VoidDump is None:
        VoidDump = {
            'runNumber': RUNNUMBER(odin_loc),
            'eventType': EVENTTYPE(odin_loc),
            'eventNumber': EVENTNUMBER(odin_loc),
        }
    if ChildCut is None:
        ChildCut = (PT > 250. * MeV)
    children = PrFilterV2Tracks(
        ChildCut, Input=make_input_tracks()['v2Sel']).Output
    if ChildDump is None:
        ChildDump = {
            'P': P,
            'PT': PT,
            'ETA': ETA,
            'PHI': PHI,
            'IP': MINIP(pv_loc),
            'IPCHI2': BPVIPCHI2(pv_loc),
            'CHI2DOF': CHI2DOF,
        }
    if VertexCut is None:
        VertexCut = (CHI2DOF < 1e3)
    if VertexDump is None:
        VertexDump = {
            'PT':
            PT,
            'ETA':
            ETA,
            'PHI':
            PHI,
            'CHI2DOF':
            CHI2DOF,
            'DIRA':
            BPVDIRA(pv_loc),
            'IPCHI2':
            BPVIPCHI2(pv_loc),
            'IPCHI2_STATE':
            BPVIPCHI2STATE(pv_loc),
            'SUMPT':
            COMB(
                Functor=SUM(PT),
                ChildContainers=[children.location],
                ChildContainerTypes=[('Pr::Selection<LHCb::Event::v2::Track>',
                                      'PrKernel/PrSelection.h')]),
        }
    if CombinationCut is None:
        CombinationCut = ALL
    if CombinationDump is None:
        CombinationDump = {
            'PT': PT,
            'DOCA': DOCA,
            'SUMPT': SUM(PT),
            'DOCACHI2': DOCACHI2,
        }
    combiner = CombineTracks(
        NBodies=2,
        VertexCut=VertexCut,
        InputTracks=children,
        CombinationCut=CombinationCut,
        VoidDump=VoidDump,
        ChildDump=ChildDump,
        CombinationDump=CombinationDump,
        VertexDump=VertexDump,
        DumpFileName=dump_filename)
    # TODO remove make_odin() from the control flow when configuration
    #      starts handling data flow in functors, see
    #      https://gitlab.cern.ch/lhcb/Moore/issues/51
    return track_mva_prefilters() + [make_odin(), combiner]
