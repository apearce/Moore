<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Run HLT1 and save an MDF file.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$HLT1CONFROOT/options/hlt1_example.py</text>
  <text>$HLT1CONFROOT/tests/options/hlt1_mdf_output.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

# Expect a single WARNING:
#   HiveDataBrokerSvc                   WARNING non-reentrant algorithm: LHCb::MDFWriter/LHCb__MDFWriter
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 1})

import re

pattern = re.compile(r'\s+NONLAZY_OR: hlt_decision\s+#=(\d+)\s+Sum=(\d+)')

# Check that:
#   1. We read at least two events
#   2. We make a positive decision on at least one event
#   3. We make a negative decision on at least one event
nread = nselected = -1
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nread, nselected = map(int, m.groups())
        break
else:
    causes.append('could not parse event statistics from stdout')

if nread &lt; 2:
    causes.append('expected at least two events to be processed')

if nselected &lt; 1:
    causes.append('expected at least one event to be selected')

if nselected == nread:
    causes.append('expected at least one event to be filtered out')

# Write out the log file so that we can compare the number of
# selected events here with the number of events processed by
# a second HLT1 job that uses the output file as input
with open('test_hlt1_persistence_mdf_write.stdout', 'w') as f:
    f.write(stdout)

</text></argument>
</extension>
