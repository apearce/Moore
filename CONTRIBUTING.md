# Contributing to Moore

Contributions to Moore are accepted in the form of merge requests. Code should
conform to the C++ and Python formatting standards define by the CI. The
checker will run automatically when you push new commits to your branch, and
will generate a patch you can apply if the checks fail.

To simplify the workflow, you can format and lint locally using

```shell
lb-format path/to/file
flake8 path/to/file
```

Check [Git4LHCb](https://twiki.cern.ch/twiki/bin/view/LHCb/Git4LHCb) for more details.

## Branches

The `master` branch is used for Run 3 development and data-taking. This is the
usual branch to open merge requests against.

Some other branches track code from previous campaigns. Open an issue if you're
unsure what branch to use.
