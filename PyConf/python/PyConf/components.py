###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Wrappers and helpers for Configurable algorithms and tools.

There are three core components that are provided for defining a dataflow:

    1. Algorithm
    2. Tool
    3. DataHandle

DataHandles are inputs and outputs of Algorithms and Tools.
"""
from __future__ import absolute_import, division, print_function
from collections import OrderedDict, defaultdict
try:
    from html import escape as html_escape
except ImportError:
    from cgi import escape as html_escape
import inspect
import re
import json

import pydot

from GaudiKernel.ConfigurableMeta import ConfigurableMeta

from . import ConfigurationError, configurable
from .dataflow import DataHandle, configurable_outputs, configurable_inputs, dataflow_config, contains_datahandle

__all__ = [
    'Algorithm',
    'Tool',
    'force_location',
    'make_algorithm',
    'make_tool',
    'contains_algorithm',
    'is_algorithm',
    'is_tool',
    'setup_component',
]

# String that separates a name from its unique ID
_UNIQUE_SEPARATOR = '#'
_UNIQUE_PREFIXES = defaultdict(lambda: 0)


def _hash_dict(d):
    """hash a dict with string keys"""
    return hash(json.dumps(d, sort_keys=True))


def _get_args(func):
    """get the argument keys of a function"""
    return inspect.getargspec(func).args


def _safe_name(string):
    """Return `string` with :: replaced with __."""
    if not string:
        return
    return string.replace('::', '__')


def _get_unique_name(prefix=''):
    """Return `prefix` appended with a unique ID.

    The ID is related to the value of `prefix` and is incremented each time
    this function is called. If the `prefix` value has not been seen before no
    ID is appended.
    """
    i = _UNIQUE_PREFIXES[prefix]
    _UNIQUE_PREFIXES[prefix] += 1
    return prefix + (_UNIQUE_SEPARATOR + str(i) if i > 0 else "")


def _strip_unique_from(name):
    """Return `name` with the unique suffix removed.

    The suffix added by `_get_unique_name`.
    """
    return re.sub(_UNIQUE_SEPARATOR + r"\d*", "", name)


def _is_configurable_algorithm(t):
    try:
        return t.getGaudiType() == 'Algorithm'
    except AttributeError:
        return False


def _is_configurable_tool(t):
    try:
        return t.getGaudiType() == 'AlgTool'
    except AttributeError:
        return False


def _check_input_integrity(t, inputs, other_args, input_transform=None):
    dh_inputs = configurable_inputs(t)
    if set(dh_inputs).intersection(other_args):
        raise TypeError(
            'Inputs must be provided as DataHandles or Algorithms, '
            'please check these arguments: {}'.format(dh_inputs))
    if not set(dh_inputs).issubset(inputs):
        raise ConfigurationError(
            'Please provide all inputs. The ones need here are: {}'.format(
                dh_inputs))
    if input_transform:
        input_transform_args = _get_args(input_transform)
        assert set(inputs).issubset(
            input_transform_args), 'input signatures do not match'


def _datahandle_ids(handles):
    """Return a tuple of IDs of the input data handles.

    If a single handle is passed, a one-tuple of its ID is returned.
    """
    handles = handles if isinstance(handles, list) else [handles]
    return tuple(h.id for h in handles)


def _gather_locations(io_dict):
    """Return the dictionary with all values mapped to their `.location` property.

    Lists values have `.location` accessed on each of their members.
    """
    d = {}
    for k, v in io_dict.items():
        if isinstance(v, list):
            d[k] = [vv.location for vv in v]
        else:
            d[k] = v.location
    return d


def _gather_tool_names(tool_dict):
    """Return the dictionary with all values mapped to their `.property_name` property."""
    return {k: v.property_name for k, v in tool_dict.items()}


def is_algorithm(arg):
    """Returns True if arg is of type Algorithm"""
    return isinstance(arg, Algorithm)


def contains_algorithm(arg):
    """Return True if arg is an Algorithm instance or list of Algorithm instances."""
    return is_algorithm(arg) or _is_list_of_algs(arg)


def _is_list_of_algs(iterable):
    """Return True if all elements are Algorithm instances.

    Returns False if the iterable is empty.
    """
    return False if not iterable else (
        isinstance(iterable, list) and all(map(contains_algorithm, iterable)))


def is_tool(arg):
    """Return True if arg is a Tool."""
    return isinstance(arg, Tool)


def _is_input(arg):
    """Return True if arg is something that produces output."""
    return contains_datahandle(arg) or contains_algorithm(arg)


def _get_input(arg):
    """Return the single input defined on arg.

    Raises an AssertionError if arg is an Algorithm with multiple inputs.
    """
    if is_algorithm(arg):
        outputs = arg.outputs.values()
        assert len(outputs) == 1
        try:
            return outputs[0]
        except KeyError:
            # outputs is a dict, so return the first and only value
            return outputs.values()[0]
    return arg


def _pop_inputs(props):
    """Return a dict of all properties that are inputs or lists of inputs."""
    inputs = OrderedDict()
    for k, v in props.items():
        if _is_input(v):
            inp = props.pop(k)
            if isinstance(inp, list):
                inp = list(map(_get_input, inp))
            else:
                inp = _get_input(inp)
            inputs[k] = inp

    return inputs


def _pop_tools(props):
    """Return a dict of all properties that are Tools.

    Raises a TypeError if a tool is not wrapped by our Tool class (e.g. a bare
    Configurable).
    """
    tools = OrderedDict()
    for k, v in props.items():
        if is_tool(v):
            tools[k] = props.pop(k)
        elif _is_configurable_tool(v):
            raise TypeError(" ".join((
                "Please either wrap your configurable with the PyConf.components.Tool wrapper",
                "or import it from PyConf.Tools if possible")))
    return tools


def _get_and_check_properties(t, props):
    """Return an OrderedDict of props.

    Raises a ConfiguurationError if any of the keys in props are not
    properties of the Algorithm/Tool t.
    """
    missing = [p for p in props if p not in t.getDefaultProperties()]
    if missing:
        raise ConfigurationError('{} are not properties of {}'.format(
            missing, t.getType()))
    return OrderedDict(props)


class force_location(str):
    """An indicator that a location must be set as defined.

    Algorithm output locations are usually appended by a hash defined that
    algorithm's properties and inputs. By wrapping an output DataHandle in
    `force_location`, this appending is not done.

    You almost never want to use this. Notable exceptions include the outputs
    of legacy unpacking algorithms, which must be defined exactly else
    SmartRefs pointing to those locations will break.
    """
    pass


class Algorithm(object):
    """An immutable wrapper around a Configurable for a Gaudi algorithm.

    An Algorithm is immutable after instantiation, so all non-default
    properties and inputs must be defined upfront. A name can be given but is
    used only as a label, not an identifier.

    Output locations are defined dynamically using a combination of the
    algorithm class name and a hash that is generated based on the algorithm
    properties and inputs. Instanting a new Algorithm that is configured
    identically to a previous Algorithm will result in the same first instance
    being returned.

    The make_algorithm method should be used to create Algorithm wrappers from
    Configurables. Importing from PyConf.Algorithms does this for you.
    """
    _algorithm_store = dict()

    _readonly = False

    def __new__(cls,
                alg_type,
                name=None,
                outputs=None,
                input_transform=None,
                output_transform=None,
                require_IOVLock=True,
                **kwargs):
        """
        Args:
            alg_type: the configurable you want to be instantiated
            name: The name to be used for the algorithm. A hash will be appended.
            outputs:
                A complete collection of outputs. This is not mandatory if
                the alg_type corresponds to an Algorithm where all outputs
                are declared via DataHandles.
                outputs can either be a list of keys ['OutputLocation1', 'OutputLocation2'],
                in case you want the framework to set locations for you.
                If you want your own locations, use a dictionary:
                {'OutputLocation1' : '/Event/OutputLocation1', ...}
                In this case, the given location will be used,
                but with a hash (to guarantee it's unique).
                In case you want to force the exact location to be used,
                wrap the location with 'force_location':
                {'OutputLocation1' : force_location('/Event/OutputLocation1'), ...}
                This might induce failures, since we rely on unique locations.
                It is not recommended, and if you use it, assure unique locations yourself!
            input_transform:
                A function to transform inputkeys/locations into actual properties.
                In case your input locations are translated into properties differently than
                {'input_key' : '<location>'},
                for instance in LoKi Functors:
                {'CODE' : 'SIZE('<location>'},
                you can specify the behaviour in input_transform.
                The arguments for input_transform are all the input keys, and the output is a
                dictionary of the resulting actual properties:
                Example: Some alg has two inputs called 'Input1' and 'Input2', and needs these
                locations to go into the 'CODE' property to check for the bank sizes
                def input_transform_for_some_alg(Input1, Input2):
                    return {'CODE' : 'SIZE({}) > 5 & SIZE({}) < 3'.format(Input1, Input2)}
            output_transform:
                Similar to input_transform.
                The output_transform arguments need to match the outputs in case you provide both.
                In case you provide only output_transform, the arguments will be used to deduce the property 'outputs'.

                output_transform functions are not allowed to modify the string
                that is interpreted as the location. They may only define how a
                location is put into the job options.
                Examples:

                1. Not allowed, as it changes the output path:

                    def transform(output):
                        return {'Output': output + '/Particles'}

                2. OK, the location is used in a LoKi functor:

                    def transform(output):
                        return {'Code': 'SIZE({})'.format(output)}

                3. OK, the location is used in a list:

                    def transform(output):
                        return {'DataKeys': [output]}

            kwargs:
                All the properties you want to set in the configurable (besides outputs)
                Every kwarg that has a DataHandle as value will be interpreted as input and
                the list of inputs needs to match the signature of 'input_transform', in case you provide it.
                Every input needs to be provided in the kwargs
                Every kwarg that has a Tool as value will be interpreted as private tool of this instance.
                Tools that have some kind of TES interation (or tools of these tools) need to be
                specified, otherwise the framework cannot know what locations to set.

        returns:
            instance of type Algorithm. It can be taken from the class store
            in case it has already been instantiated with the same configuration
        """
        if not _is_configurable_algorithm(alg_type):
            raise TypeError(
                'cannot declare an Algorithm with {}, which is of type {}'.
                format(alg_type, alg_type.getGaudiType()))
        #INPUTS ###########
        # TODO when everything is functional, input keys can be checked!
        _inputs = _pop_inputs(kwargs)
        _check_input_integrity(alg_type, _inputs, kwargs, input_transform)

        # We normally assume that an algorithms properties and input locations
        # fully define its behaviour, but when the user forces an output
        # location this forms part of the algorithm's defined behaviour
        # So, record when any output location is forced
        if isinstance(outputs, dict):
            forced_locations = {
                key: str(output)
                for key, output in outputs.items()
                if isinstance(output, force_location)
            }
        else:
            forced_locations = dict()

        #TOOLS ##############
        _tools = _pop_tools(kwargs)

        #PROPERTIES ############
        _properties = _get_and_check_properties(alg_type, kwargs)

        #HASH ###########
        identity = cls._calc_id(alg_type.getType(), _properties, _inputs,
                                _tools, forced_locations)

        # return the class if it exists already, otherwise create it
        try:
            instance = cls._algorithm_store[identity]
            if name and _strip_unique_from(instance._name) != name:
                raise ConfigurationError(
                    "cannot instantiate the same algorithm twice with different names"
                )
        except KeyError:
            instance = super(Algorithm, cls).__new__(cls)

            # __init__ basically
            instance._id = identity
            instance._alg_type = alg_type
            instance._name = _get_unique_name(name or alg_type.getType())
            instance._inputs = _inputs
            instance._input_transform = input_transform
            instance._output_transform = output_transform
            instance._properties = _properties
            instance._requireIOV = require_IOVLock
            instance._tools = _tools
            instance._outputs_define_identity = bool(forced_locations)

            #make OUTPUTS ############# these were not needed for calculation of the ID (only the forced locations)
            def _make_outs(outputs):
                # special behaviour if outputs is a dict: let them have readable locations
                # with force_location you can force this exact location (unsafe, usecase rawevent)
                if isinstance(outputs, dict):
                    return {
                        k: DataHandle(instance, k, v)
                        for k, v in outputs.items()
                    }
                return {k: DataHandle(instance, k) for k in outputs}

            instance._outputs = {}
            if outputs:
                instance._outputs = _make_outs(outputs)
            if instance._output_transform:
                output_transform_args = _get_args(instance._output_transform)
                if not outputs:
                    instance._outputs = _make_outs(output_transform_args)
                else:
                    assert set(output_transform_args).issubset(
                        instance._outputs), 'output signatures do not match'

            if not instance._outputs:
                # neither explicit outputs nor a transform was set
                # we can still deduce from functional signature (hopefully)
                instance._outputs = _make_outs(
                    configurable_outputs(instance.type))

            for o in instance._outputs:
                if o in kwargs:
                    raise ConfigurationError(
                        "Cannot set output property {} explicitly. \
                                          Please configure the 'outputs' property correctly."
                        .format(o))

            for key, src in instance._outputs.items():
                setattr(instance, key, src)

            instance._readonly = True
            cls._algorithm_store[identity] = instance
        #return the cached or new instance
        return instance

    @staticmethod
    def _calc_id(typename, props, inputs, tools, forced_outputs=None):
        if forced_outputs is None:
            forced_outputs = dict()
        props_hash = _hash_dict(props)
        # TODO include the transformed input somehow
        inputs_hash = _hash_dict(
            {key: _datahandle_ids(handles)
             for key, handles in inputs.items()})
        tools_hash = _hash_dict({key: tool.id for key, tool in tools.items()})
        outputs_hash = _hash_dict(forced_outputs)
        to_be_hashed = (typename, props_hash, inputs_hash, tools_hash,
                        outputs_hash)
        return hash(to_be_hashed)

    # end of __init__

    @property
    def inputs(self):
        return self._inputs

    @property
    def outputs(self):
        return self._outputs

    @property
    def properties(self):
        return self._properties

    @property
    def name(self):
        return _safe_name(self._name)

    @property
    def fullname(self):
        return self.typename + '/' + self.name

    @property
    def id(self):
        return self._id

    @property
    def type(self):
        return self._alg_type

    @property
    def tools(self):
        return self._tools

    @property
    def typename(self):
        return _safe_name(self.type.getType())

    def __repr__(self):
        return self.fullname

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def configuration(self):
        config = dataflow_config()

        #children
        for inputs in self.inputs.values():
            inputs = inputs if isinstance(inputs, list) else [inputs]
            for inp in inputs:
                config.update(inp.producer.configuration())

        for tool in self.tools.values():
            config.update(tool.configuration(self.name))

        #props
        cfg = config[(self.type, self.name)] = self.properties.copy()
        cfg[config.iovlockkey] = self._requireIOV  # FIXME

        #io
        input_dict = _gather_locations(self.inputs)
        output_dict = _gather_locations(self.outputs)

        if self._input_transform:
            input_dict = self._input_transform(**input_dict)

        if self._output_transform:
            output_dict = self._output_transform(**output_dict)

        cfg.update(input_dict)
        cfg.update(output_dict)

        #tools
        tools_dict = _gather_tool_names(self.tools)
        cfg.update(tools_dict)

        return config

    def _graph(self, graph):
        """Add our dataflow as a `pydot.Subgraph` to graph.

        Parameters
        ----------
        graph -- pydot.Graph
        """

        # TODO deduplicate graphing?
        # in all diamond structures, the subgraph is built multiple times

        def format_prop(name, value, max_len=100):
            assert max_len > 15, 'max_len should be at least 15'
            value = str(value)
            if len(value) > max_len:
                value = (value[:max_len // 2] + '[...]' +
                         value[max_len // 2 + 5:max_len])
            return '{} = {}'.format(name, value)

        #inner part ########
        own_name = html_escape(self.fullname)
        sg = pydot.Subgraph(graph_name='cluster_' + own_name)
        sg.set_label('')
        sg.set_bgcolor('palegreen1')

        props = self._properties
        # Include output locations when they define algorithm behaviour
        if self._outputs_define_identity:
            output_props = {k: v.location for k, v in self._outputs.items()}
            props = dict(props.items() + output_props.items())
        props_str = '<BR/>'.join(
            html_escape(format_prop(k, v)) for k, v in props.items())
        label = ('<<B>{}</B><BR/>{}>'.format(own_name, props_str
                                             or 'defaults-only'))
        gnode = pydot.Node(own_name, label=label, shape='plaintext')
        sg.add_node(gnode)

        #IO for the inner part
        for name in self.inputs:
            input_id = html_escape('{}_in_{}'.format(self.fullname, name))
            node = pydot.Node(
                input_id,
                label=html_escape(name),
                fillcolor='deepskyblue1',
                style='filled')
            edge = pydot.Edge(gnode, node, style='invis', minlen='0')
            sg.add_node(node)
            sg.add_edge(edge)
        for name in self.outputs:
            output_id = html_escape('{}_out_{}'.format(self.fullname, name))
            node = pydot.Node(
                output_id,
                label=html_escape(name),
                fillcolor='coral1',
                style='filled')
            edge = pydot.Edge(gnode, node, style='invis', minlen='0')
            sg.add_node(node)
            sg.add_edge(edge)

        # tool inputs
        def _inputs_from_tool(tool):
            inputs = {tool.name: tool.inputs}
            for _tool in tool.tools.values():
                inputs.update(_inputs_from_tool(_tool))
            return inputs

        tool_inputs = OrderedDict()
        for tool in self.tools.values():
            tool_inputs.update(_inputs_from_tool(tool))

        for toolname, inputs in tool_inputs.items():
            for name in inputs:
                input_id = html_escape('{}_in_{}'.format(toolname, name))
                label = ('<<B>{}</B><BR/>from {}>'.format(name, toolname))
                node = pydot.Node(
                    input_id,
                    label=label,
                    fillcolor='deepskyblue1',
                    style='filled')
                edge = pydot.Edge(style='invis', minlen='0')
                sg.add_edge(gnode, node)

        graph.add_subgraph(sg)

        # external links #######
        for key, handle in self.inputs.items():
            edge = pydot.Edge(
                html_escape('{}_out_{}'.format(handle.producer.fullname,
                                               handle.key)),
                html_escape('{}_in_{}'.format(self.fullname, key)))
            graph.add_edge(edge)
            handle.producer._graph(graph)

        for toolname, inputs in tool_inputs.items():
            for name, handle in inputs.items():
                edge = pydot.Edge(
                    html_escape('{}_out_{}'.format(handle.producer.fullname,
                                                   handle.key)),
                    html_escape('{}_in_{}'.format(toolname, name)))
                graph.add_edge(edge)

    def plot_dataflow(self):
        """Return a `pydot.Dot` of the dataflow defined by this Algorithm."""
        top = pydot.Dot(graph_name=self.fullname, strict=True)
        top.set_node_defaults(shape='box')
        self._graph(top)
        return top

    def __setitem__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setitem__(self, k, v)

    def __setattr__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setattr__(self, k, v)

    def __delitem__(self, i):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delitem__(self, i)

    def __delattr__(self, a):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delattr__(self, a)


# TODO(AP): maybe wrap_algorithm is better
def make_algorithm(alg_type,
                   defaults=None,
                   input_transform=None,
                   output_transform=None,
                   outputs=None,
                   require_IOVLock=True):  # FIXME: iovlock
    """Return the Configurable class alg_type wrapped as an Algorithm.

    See the Algorithm.__new__ method for details on the keyword arguments.
    """
    if not _is_configurable_algorithm(alg_type):
        raise TypeError(
            'cannot declare an Algorithm with {}, which is of type {}'.format(
                alg_type, alg_type.getGaudiType()))

    if defaults is None:
        defaults = {}

    @configurable
    def wrapped(**kwargs):
        props = dict(defaults, **kwargs)
        return Algorithm(
            alg_type,
            outputs=outputs,
            input_transform=input_transform,
            output_transform=output_transform,
            require_IOVLock=require_IOVLock,
            **props)

    return wrapped


class Tool(object):
    """An immutable wrapper around a Configurable for a Gaudi tool.

    A Tool is immutable after instantiation, so all non-default properties and
    inputs must be defined upfront. A name can be given but is used only as a
    label, not an identifier.

    Instanting a new Tool that is configured identically to a previous
    Tool will result in the same first instance being returned.

    The make_tool method should be used to create Tool wrappers from
    Configurables. Importing from PyConf.Tools does this for you.
    """

    _tool_store = dict()

    _readonly = False

    def __new__(cls, tool_type, name=None, **kwargs):
        """
        Args:
            tool_type: the configurable you want to be instantiated
            name: The name to be used for the Tool. A hash will be appended.
            kwargs:
                All the properties you want to set in the configurable (besides outputs)
                Every kwarg that has a DataHandle as value will be interpreted as input.
                Every input needs to be provided in the kwargs.
                Every kwarg that has a Tool as value will be interpreted as private tool of this instance.
                Tools that have some kind of TES interation (or tools of these tools) need to be
                specified, otherwise the framework cannot know what locations to set.
        returns:
            instance of type Tool, maybe taken from the tool store (in case of same configuration)
        """
        if not _is_configurable_tool(tool_type):
            raise TypeError(
                'cannot declare a Tool with {}, which is of type {}'.format(
                    tool_type, tool_type.getGaudiType()))

        #inputs
        _inputs = _pop_inputs(kwargs)
        _check_input_integrity(tool_type, _inputs, kwargs)

        #tools
        _tools = _pop_tools(kwargs)

        #properties
        _properties = _get_and_check_properties(tool_type, kwargs)

        #calculate the id, this determines whether we can take an already created instance
        identity = cls._calc_id(tool_type.getType(), _properties, _inputs,
                                _tools)
        try:
            instance = cls._tool_store[identity]
            if name and _strip_unique_from(instance._name) != name:
                raise ConfigurationError(
                    "cannot instantiate the same tool twice with different names"
                )
        except KeyError:
            instance = super(Tool, cls).__new__(cls)
            instance._id = identity
            instance._tool_type = tool_type
            instance._name = _get_unique_name(name or tool_type.getType())
            instance._inputs = _inputs
            instance._properties = _properties
            instance._tools = _tools
            instance._readonly = True
            cls._tool_store[identity] = instance
        return instance

    @staticmethod
    def _calc_id(typename, props, inputs, tools):
        props_hash = _hash_dict(props)
        inputs_hash = _hash_dict(
            {key: _datahandle_ids(handles)
             for key, handles in inputs.items()})
        tools_hash = _hash_dict({key: tool.id for key, tool in tools.items()})
        to_be_hashed = (typename, props_hash, inputs_hash, tools_hash)
        return hash(to_be_hashed)

    @property
    def inputs(self):
        return self._inputs

    @property
    def properties(self):
        return self._properties

    @property
    def id(self):
        return self._id

    @property
    def tools(self):
        return self._tools

    @property
    def property_name(self):
        #how instances refer to their tools
        return self.typename + '/' + self._name

    def name(self, parent=None):
        #configuration name of the tool itself
        return (parent or "ToolSvc") + \
               '.' + self._name

    @property
    def type(self):
        return self._tool_type

    @property
    def typename(self):
        return self.type.getType()

    def __repr__(self):
        return 'Tool({})'.format(self.name())

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def configuration(self, parent=None):
        name = self.name(parent)
        config = dataflow_config()

        for inputs in self.inputs.values():
            inputs = inputs if isinstance(inputs, list) else [inputs]
            for inp in inputs:
                config.update(inp.producer.configuration())

        for tool in self.tools.values():
            config.update(tool.configuration(name))

        cfg = config[(self.type, name)] = self.properties.copy()
        input_dict = _gather_locations(self.inputs)
        cfg.update(input_dict)

        tools_dict = _gather_tool_names(self.tools)
        cfg.update(tools_dict)

        return config

    def __setitem__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setitem__(self, k, v)

    def __setattr__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setattr__(self, k, v)

    def __delitem__(self, i):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delitem__(self, i)

    def __delattr__(self, a):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delattr__(self, a)


# TODO(AP): maybe wrap_tool is better
def make_tool(tool_type, defaults=None):
    """Return the Configurable class tool_type wrapped as a Tool.

    See the Tool.__new__ method for details on the keyword arguments.
    """
    if not _is_configurable_tool(tool_type):
        raise TypeError(
            'cannot declare a Tool with {}, which is of type {}'.format(
                tool_type, tool_type.getGaudiType()))

    if defaults is None:
        defaults = {}

    @configurable
    def wrapped(**kwargs):
        props = dict(defaults, **kwargs)
        return Tool(tool_type, **props)

    return wrapped


def setup_component(alg,
                    instanceName=None,
                    packageName='Configurables',
                    IOVLockDep=False,
                    **kwargs):
    """Return an instance of the class alg.

    If alg is a string, import the named class from packageName.

    If IOVLockDep is True, a dependency on `/Event/IOVLock` is added to the
    instance's ExtraInputs property.

    Additional keyword arguments are forwarded to the alg constructor.
    """
    if isinstance(alg, ConfigurableMeta):
        return alg((instanceName or alg.name()), **kwargs)
    imported = getattr(__import__(packageName, fromlist=[alg]), alg)
    instance = imported((instanceName or alg), **kwargs)
    if IOVLockDep and hasattr(
            instance,
            'ExtraInputs') and '/Event/IOVLock' not in instance.ExtraInputs:
        instance.ExtraInputs.append('/Event/IOVLock')  # FIXME
    return instance
