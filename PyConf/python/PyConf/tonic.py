###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Wrappers for defining functions that can be configured higher up the call stack.

Tonic provides the `@configurable` decorator, which allows the default values
of keyword arguments to be overridden from higher up the callstack with
`function.bind`.

    >>> from PyConf import configurable
    >>> @configurable
    ... def f(a=1):
    ...     return a
    ...
    >>> with f.bind(a=2):
    ...     f()
    ...
    2
    >>> f()
    1

This allows for high-level configuration of behaviour deep within an
application; all that's needed is a reference to the `@configurable` function
that one wishes to modify the behaviour of.

The idiomatic way of using tonic is define small, self-contained functions
which construct some object of interest. These functions should call other,
similarly self-contained functions to retrieve any components which are
dependencies. Like this, callers can override the behaviour of any function in
the call stack with `.bind`. Each function should expose configurable
parameters as keyword arguments.

To help debugging, bindings can be inspected using the `debug` context manager.

    >>> from PyConf import tonic
    >>> with tonic.debug():
    ...     f()
    ...     f(a=3)
    ...     with f.bind(a=2):
    ...         f()
    ...
    Calling @configurable `f` from <module>:2 with non-default parameters: NONE
    1
    Calling @configurable `f` from <module>:3 with non-default parameters:
        a = 3 (given)
    3
    Calling @configurable `f` from <module>:5 with non-default parameters:
        a = 2 (bound)
    2

tonic is named for Google's gin configuration framework [1] which served as
inspiration.

[1]: https://github.com/google/gin-config
"""
from __future__ import absolute_import, division, print_function
import inspect
import wrapt
import warnings
from functools import partial
from itertools import islice
try:
    from itertools import izip
except ImportError:
    # In Python 3, zip already returns an iterator
    izip = zip
from contextlib import contextmanager

__configurables = set()
__namespace_stack = []
__bind_scope_stack = []
__debug = False

DEFAULT_NAMESPACE_SELECTOR = ()


def _full_name(x):
    return (x.__module__ or "main") + '.' + x.__name__


@contextmanager
def _bind(configurable, *args, **kwargs):
    """Bind a value to the parameters of a configurable.

    The changes made to the default argument values implied by `.bind(...)` are
    only valid within the scope that the `.bind(...)` call is made. The changes
    made by `bind` then go 'out of scope' when leaving the scope.

    Scoping is implemented as a context manager.

    Args:
        configurable: configurable function to bind to
        namespace_selector (str, optional): '/'-separated namespaces
        **kwargs: parameters and values

    """
    if len(args) > 1:
        raise TypeError('bind takes exactly one or two positional arguments')
    namespace_selector = tuple(
        args[0].split('/') if args and args[0] else DEFAULT_NAMESPACE_SELECTOR)

    if configurable not in __configurables:
        raise ValueError('The argument of bind is not a configurable')

    try:
        spec = inspect.getfullargspec(configurable)
        spec_kwargs = spec.kwonlyargs
    except AttributeError:
        # Python 2 doesn't have getfullargspec
        spec = inspect.getargspec(configurable)
        spec_kwargs = spec.keywords
    for param_name, param_value in kwargs.items():
        if not spec_kwargs and param_name not in spec.args:
            raise ValueError("{} does not have a parameter '{}'".format(
                configurable, param_name))
            # TODO how can we do type checking here on `value`?

    binding_namespace = tuple(__namespace_stack)
    # TODO need to check the selector doesn't already contain the binding
    # ns; it's appended automatically so the user shouldn't give it
    namespace_selector = binding_namespace + namespace_selector
    # TODO Binds only affect calls in the same stack or deeper, including
    # namespace 'stacks', so if we bind to a namespace we're already *not*
    # inside, we never will be in this stack, so it will have no effect
    # The namespace selector must then be at the the same current namespace
    # or deeper
    if not namespace_matches(namespace_selector, binding_namespace):
        # TODO add test
        warnings.warn(('Binding to namespace {} when inside namespace {}; '
                       'this will never match').format(namespace_selector,
                                                       binding_namespace))

    # TODO record stack trace and show in errors
    # TODO can we detect some overriding selectors already here?
    __bind_scope_stack.append((configurable, namespace_selector, kwargs))
    try:
        yield
    finally:
        __bind_scope_stack.pop()


# TODO contextmanager loses the signature, replace it
@contextmanager
def namespace(name):
    """Create a named context that is appended to the namespace stack.

    A `bind` that is bound to a specific namespace will only affect function
    calls made within that namespace.

        >>> @configurable
        ... def example(param=1):
        ...     return param
        ...
        >>> example()
        1
        >>> with example.bind('my_scope', param=2):
        ...     example()
        ...
        1
        >>> with example.bind('my_scope', param=2):
        ...     with namespace('my_scope'):
        ...         example()
        ...
        2
    """
    __namespace_stack.append(name)
    try:
        yield __namespace_stack
    finally:
        __namespace_stack.pop()


def namespace_matches(namespace_stack, namespace_selector):
    """Return whether a namespace matches a selector.

    The tuple `namespace_stack` is considered a match if it ends with the tuple
    `namespace_selector`.

        >>> namespace_matches(('a', 'b'), ('a', 'b'))
        True
        >>> namespace_matches(('a', 'b'), ('b',))
        True
        >>> namespace_matches(('a', 'b'), tuple())
        True
        >>> namespace_matches(('a', 'b'), ('a',))
        False
        >>> namespace_matches(('a', 'b'), ('a', 'b', 'c'))
        False

    The stack represents the 'current' namespace. A selector matches a stack
    from the bottom up.
    """
    if len(namespace_stack) < len(namespace_selector):
        return False
    end = islice(namespace_stack,
                 len(namespace_stack) - len(namespace_selector), None)
    return all(a == b for a, b in izip(end, namespace_selector))


def _bound_parameters(configurable,
                      namespace_stack,
                      bind_scope_stack,
                      _stacklevel=2):
    """Return the parameters bound to configurable given the namespace and scope stacks."""
    # TODO shall we start from defaults here?
    matches = {}
    # Going from top to bottom matches the behaviour where deeper binds
    # override higher-level binds
    # TODO: is this what we want?
    for bound_configurable, namespace_selector, params in bind_scope_stack:
        if bound_configurable != configurable:
            continue
        if not namespace_matches(namespace_stack, namespace_selector):
            continue
        for param, value in params.items():
            if param in matches and value != matches[param]:
                warnings.warn(
                    'multiple matches: {!r} overrides {!r}'.format(
                        value, matches[param]),
                    stacklevel=_stacklevel)
                # TODO more debugging information, e.g. where were
                # these overriding parameters bound
            matches[param] = value

    return matches


def bound_parameters(configurable):
    """Return the parameters bound to configurable in the current stack scope."""
    return _bound_parameters(
        configurable, __namespace_stack, __bind_scope_stack, _stacklevel=3)


def _configurable_wrapper(wrapped, _, args, kwargs):
    """Wrapper for methods marked @configurable."""
    if args:
        try:
            wrapped_args = inspect.getfullargspec(wrapped).args
        except AttributeError:
            wrapped_args = inspect.getargspec(wrapped).args
        if len(args) > len(wrapped_args):
            raise TypeError(
                'too many positional arguments given, expected <{}, gave {}'.
                format(len(wrapped_args), len(args)))
        named_args = dict(zip(wrapped_args, args))
        dupls = set(kwargs).intersection(named_args)
        if dupls:
            raise TypeError('{} got multiple values for {}'.format(
                wrapped, dupls))
        kwargs.update(named_args)

    overrides = bound_parameters(wrapped)
    # TODO flag used parameters and warn for unused ones (when?)
    # TODO also warn/debug about overrides here
    dupls = set(kwargs).intersection(overrides)
    for dup in dupls:
        warnings.warn('{}.{} defined via bind, using bound value {}'.format(
            wrapped, dup, overrides[dup]))
    kwargs.update(overrides)

    # Prepare the debugging print-out
    current_frame = inspect.currentframe()
    caller_frame = inspect.getouterframes(current_frame, 2)[1]
    descriptors = []
    for pname in sorted(kwargs.keys()):
        # Show how we arrived at the value we're going to use
        if pname in overrides:
            # Value was specified with `bind`
            ptype = 'bound'
        elif pname in kwargs:
            # Value was given at the call site
            ptype = 'given'
        else:
            # Default value is used
            ptype = 'default'
        d = '{} = {} ({})'.format(pname, kwargs[pname], ptype)
        descriptors.append(d)
    params = (
        '\n    ' + '\n    '.join(descriptors)) if descriptors else ' NONE'
    log('Calling @configurable `{func}` from {file}:{line} with non-default parameters:{params}'
        .format(
            func=wrapped.__name__,
            file=caller_frame[3],
            line=caller_frame[2],
            params=params))

    return wrapped(**kwargs)


def configurable(wrapped=None):
    """Mark a function as configurable.

    The behaviour of a configurable function can be modified using the bind
    syntax:

        >>> @configurable
        ... def f(a=1):
        ...     return a
        ...
        >>> with f.bind(a=2):
        ...     f()
        ...
        2
        >>> f()
        1
    """
    if wrapped is None:
        # the function to be decorated hasn't been given yet
        # so we just collect the optional keyword arguments.
        return partial(configurable)

    __configurables.add(wrapped)
    wrapped.bind = partial(_bind, wrapped)
    # TODO better docs of wrapped.bind

    return wrapt.FunctionWrapper(
        wrapped=wrapped, wrapper=_configurable_wrapper)


@contextmanager
def debug():
    """Context manager that enables debug messaging from tonic.

        >>> log('A message')  # no print-out
        >>> with debug():
        ...     log('A second message')
        ...
        A second message
    """
    global __debug
    __debug = True
    yield
    __debug = False


def log(msg):
    """Prints a message if the __debug flag is True."""
    if __debug:
        print(msg)
