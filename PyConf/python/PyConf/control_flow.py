###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
try:
    from html import escape as html_escape
except ImportError:
    from cgi import escape as html_escape

from enum import Enum
import pydot

from .components import Algorithm
from .dataflow import DataHandle

__all__ = [
    'NodeLogic',
    'CompositeNode',
]


# FIXME not sure if i want to have this or rather just strings
class NodeLogic(Enum):
    """Node control flow behaviour.

    Each node contains an ordered set of subnodes/child nodes. These are
    processed in order one by one until the node can return True or False.
    Whether a node can return depends on its control flow behaviour.
    """
    # Return False and stop processing as soon as a subnode returns False
    LAZY_AND = 'LAZY_AND'
    # Return False if any subnode returns False, but do process all subnodes
    NONLAZY_AND = 'NONLAZY_AND'
    # Return True and stop processing as soon as a subnode returns True
    LAZY_OR = 'LAZY_OR'
    # Return True if any subnode returns True, but do process all subnodes
    NONLAZY_OR = 'NONLAZY_OR'
    # Return the negation of the subnode
    NOT = 'NOT'


class CompositeNode(object):
    """A container for a set of subnodes/child nodes."""

    def __init__(self,
                 name,
                 children,
                 combineLogic=NodeLogic.LAZY_AND,
                 forceOrder=False):
        if not isinstance(combineLogic, NodeLogic):
            raise TypeError('combineLogic must take an instance of NodeLogic')
        self.name = name
        self.children = [
            c.producer if isinstance(c, DataHandle) else c for c in children
        ]  # <- is this the right way?
        self.combineLogic = combineLogic
        self.forceOrder = forceOrder

    def __eq__(self, other):  # TODO maybe not needed
        return self.name == other.name and \
            self.children == other.children and \
            self.combineLogic == other.combineLogic and \
            self.forceOrder == other.forceOrder

    @property  # for API compatibility with Algorithm
    def fullname(self):
        return self.name

    def represent(self):
        return (self.name, self.combineLogic.value,
                [c.fullname for c in self.children], self.forceOrder)

    def _graph(self, graph):
        own_name = html_escape(self.name)
        sg = pydot.Subgraph(graph_name='cluster_' + own_name)

        label = ('<<B>{}</B><BR/>{}, {}>'.format(
            own_name,
            str(self.combineLogic).replace('NodeLogic.', ''),
            'ordered' if self.forceOrder else 'unordered'))
        sg.set_label(label)
        sg.set_edge_defaults(dir='forward' if self.forceOrder else 'none')

        prev_node = None
        for child in self.children:
            if isinstance(child, Algorithm):
                # Must name nodes uniquely within a node, otherwise they will
                # only be drawn one (which makes sense for the *data* flow!)
                node = pydot.Node(
                    html_escape('{}_{}'.format(self.name, child.fullname)),
                    label=child.fullname)
                sg.add_node(node)
            else:
                node = child._graph(sg)

            if prev_node is not None:
                # When drawing edges to/from subgraphs, the target node must be
                # a node inside the subgraph, which we take as the first.
                # However we want the arrow to start/from from the edge of the
                # subgraph, so must set the ltail/lhead attribute appropriately
                if isinstance(prev_node, pydot.Subgraph):
                    tail_node = prev_node.get_nodes()[0]
                    ltail = prev_node.get_name()
                else:
                    tail_node = prev_node
                    ltail = None
                if isinstance(node, pydot.Subgraph):
                    head_node = node.get_nodes()[0]
                    lhead = node.get_name()
                else:
                    head_node = node
                    lhead = None
                edge = pydot.Edge(tail_node, head_node)
                if ltail is not None:
                    edge.set_ltail(ltail)
                if lhead is not None:
                    edge.set_lhead(lhead)
                sg.add_edge(edge)

            prev_node = node

        graph.add_subgraph(sg)

        return sg
