###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import datetime
import logging

import pydot

from Configurables import (
    CallgrindProfile,
    DeterministicPrescaler,
    DDDBConf,
    CondDB,
    # FIXME(NN): We shouldn't need to refer to IOV explicitly in our framework
    LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV,
    LHCb__Tests__FakeEventTimeProducer as DummyEventTime,
)
from Configurables import LHCb__MDFWriter, InputCopyStream, OutputStream
from Gaudi.Configuration import ConfigurableUser, DEBUG
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

from . import ConfigurationError
from .components import Algorithm, setup_component, is_algorithm, is_tool
from .control_flow import CompositeNode, NodeLogic
from .dataflow import dataflow_config

__all__ = [
    'EverythingHandler',
]

log = logging.getLogger(__name__)
# Writer algorithm and configuration used for a given file type
KNOWN_WRITERS = {
    'MDF': (LHCb__MDFWriter, dict(BankLocation='/Event/DAQ/RawEvent')),
    # FIXME(AP): InputCopyStream is broken because it uses incidents to
    # try to ensure all data it writes originated from the same input
    # file
    # 'ROOT': (InputCopyStream, dict()),
    'ROOT': (OutputStream, dict(ItemList=['/Event/DAQ/RawEvent#1'])),
}


def _is_node(arg):
    return isinstance(arg, CompositeNode)


def _output_writer(writer_cls, filename, **kwargs):
    """Return a configured file writer instance.

    Parameters
    ----------
    writer_cls : PyConf.components.Algorithm
        File writer algorithm class to use. An instance of this is returned,
        configured to write a file named `filename`.
    filename : str
        Path of the file to be written.
    """
    if writer_cls in [InputCopyStream, OutputStream]:
        output = "DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
            filename)
        kwargs['Output'] = output
    elif writer_cls == LHCb__MDFWriter:
        connection = 'file://{}'.format(filename)
        kwargs['Connection'] = connection
    else:
        assert False, 'Do not know writer type {}'.format(writer_cls)
    return writer_cls(**kwargs)


def setupInput(inputFiles,
               dataType,
               DDDBTag,
               CONDDBTag,
               Simulation,
               inputFileType,
               outputFileType=None,
               withEventSelector=True,
               TESName='HiveWhiteBoard'):
    # FIXME(AP) we need to modify the ApplicationMgr and query the
    # HiveWhiteBoard held by the EverythingHandler; the former modifies global
    # state which is not ideal
    whiteboard = setup_component(TESName, instanceName='EventDataSvc')
    if inputFileType != 'MDF' and whiteboard.EventSlots > 1:
        raise ConfigurationError(
            "only MDF files can run in multithreaded mode, please change number of eventslots to 1"
        )
    setup_component(
        'ApplicationMgr',
        packageName='Gaudi.Configuration',
        EvtSel="EventSelector" if withEventSelector else "NONE")
    setup_component('DDDBConf', Simulation=Simulation, DataType=dataType)
    setup_component(
        'CondDB', Upgrade=True, Tags={
            'DDDB': DDDBTag,
            'SIMCOND': CONDDBTag
        })
    if not withEventSelector: return inputFiles
    input_iohelper = IOHelper(inputFileType, outputFileType)
    input_iohelper.setupServices()
    evtSel = input_iohelper.inputFiles(inputFiles, clear=True)
    inputs = []
    for inp in evtSel.Input:
        inputs.append(inp + " IgnoreChecksum='YES'")
    evtSel.Input = inputs
    evtSel.PrintFreq = 10000
    setup_component('IODataManager', DisablePFNWarning=True)
    return inputs


def setupInputFromTestFileDB(testFileDBkey,
                             inputFiles=None,
                             fileType=None,
                             outputFileType=None,
                             withEventSelector=True,
                             TESName='HiveWhiteBoard'):
    """Run from files defined by a TestFileDB key.

    Parameters
    ----------
    testFileDBkey : str
    inputFiles : list of str
        Overrides the input files defined by the TestFileDB entry.
    fileType : str
    """
    if inputFiles is not None and fileType is None:
        raise ValueError('Must specify fileType when inputFiles is specified')

    qualifiers = test_file_db[testFileDBkey].qualifiers
    dataType = qualifiers['DataType']
    Simulation = qualifiers['Simulation']
    fileType = fileType or 'ROOT' if qualifiers[
        'Format'] != 'MDF' else qualifiers['Format']
    CondDBTag = qualifiers['CondDB']
    DDDBTag = qualifiers['DDDB']
    if not inputFiles:
        inputFiles = test_file_db[testFileDBkey].filenames
    return setupInput(inputFiles, dataType, DDDBTag, CondDBTag, Simulation,
                      fileType, outputFileType, withEventSelector, TESName)


def setupOutput(filename, filetype, TESName='HiveWhiteBoard'):
    """Configure the application to write out a file.

    Only the raw event, under `/Event/DAQ/RawEvent`, is persisted.

    Must be called after `EverythingHandler.configure` has been called.

    Parameters
    ----------
    filename : str
    filetype : str
        One of the keys of `KNOWN_WRITERS`.
    """
    # FIXME(AP) we need to modify the HLTControlFlowMgr and query the
    # HiveWhiteBoard held by the EverythingHandler; the former modifies global
    # state which is not ideal
    whiteboard = setup_component('HiveWhiteBoard', instanceName='EventDataSvc')
    scheduler = setup_component('HLTControlFlowMgr')
    assert whiteboard.EventSlots == 1, 'Cannot write output multithreaded'
    assert scheduler.ThreadPoolSize == 1, 'Cannot write output multithreaded'

    assert filetype in KNOWN_WRITERS, 'Output filetype not supported: {}'.format(
        filename)
    writer_cls, configuration = KNOWN_WRITERS[filetype]

    writer = _output_writer(writer_cls, filename, **configuration)

    # FIXME(AP) Hack to add the writer to the overall control flow
    # This causes the writer to only run when the HLT decision is positive
    for node_name, _, members, _ in scheduler.CompositeCFNodes:
        if node_name == 'HLT':
            assert len(members) == 1, 'setupOutput called twice'
            members.append(writer.getFullName())
            break
    else:
        raise ConfigurationError(
            'Must called EverythingHandler.configure before setupOutput')

    hiveDataBroker = setup_component('HiveDataBrokerSvc')
    hiveDataBroker.DataProducers.append(writer.getFullName())

    return writer


def _gaudi_datetime_format(dt):
    """Return the datetime object formatted as Gaudi does it.

    As seen in the application "Welcome" banner.
    """
    return dt.strftime('%a %h %d %H:%M:%S %Y')


class PythonLoggingConf(ConfigurableUser):
    """Takes care of configuring the python logging verbosity."""
    # Make sure we're applied before anything else by listing
    # configurables in __used_configurables__. This ensures that we can
    # modify the python logging level before anything spits out messages.
    __used_configurables__ = [
        CondDB,
        DDDBConf,
    ]

    def __apply_configuration__(self):
        import GaudiKernel.ProcessJobOptions
        # turn off printing for the rest of the configuration
        log.info('Disabling info messages from python logging')
        GaudiKernel.ProcessJobOptions.PrintOff()


class EverythingHandler(object):
    """The high level application configurator.

    It:

    - Sets up global variables like the number of events to process, the number
      of threads to run with, which input files to use, and the database tags
    - Sets up services needed for the application, like the scheduler and
      application manager
    - Provides an interface to schedule parallel NONLAZY_OR selections (e.g. a
      set of trigger lines)
    - Translates the data and control flow configuration into job options
      via `configure`
    - can plot your data flow

    The current implementation is quite specific to the needs of an HLT-like
    application.
    """

    def __init__(self,
                 threadPoolSize=1,
                 nEventSlots=0,
                 evtMax=-1,
                 debug=True,
                 HistoFile=None,
                 TESName='HiveWhiteBoard'):
        self._nodes = []
        self._algs = []
        self._tools = []  # public tools
        self._lines = []
        # FIXME we make the configurables accessible to be able to hack the ugly things
        if nEventSlots == 0:
            nEventSlots = int(threadPoolSize * 1.2)  # sensible default
        self._whiteboard = setup_component(
            TESName,
            instanceName='EventDataSvc',
            EventSlots=nEventSlots,
            ForceLeaves=True)
        self._scheduler = setup_component(
            'HLTControlFlowMgr',
            # sequence to be filled by defineSequence
            CompositeCFNodes=[],
            ThreadPoolSize=threadPoolSize)
        self._appMgr = setup_component(
            'ApplicationMgr',
            packageName='Gaudi.Configuration',
            EventLoop=self._scheduler,
            EvtMax=evtMax,
            EvtSel="NONE")
        self._appMgr.ExtSvc.insert(0, self._whiteboard)
        setup_component('UpdateManagerSvc', WithoutBeginEvent=True)
        self._hiveDataBroker = setup_component('HiveDataBrokerSvc')
        setup_component('AlgContextSvc', BypassIncidents=True)  # for LoKi
        setup_component('LoKiSvc', Welcome=False)
        setup_component('MessageSvc', Format='% F%35W%S %7W%R%T %0W%M')
        setup_component('HistogramPersistencySvc', OutputLevel=5)
        self._eventClockSvc = setup_component(
            'EventClockSvc', InitialTime=1433509200000000000)
        if HistoFile is not None:
            self._histogramPersistencySvc = setup_component(
                'HistogramPersistencySvc', OutputFile=HistoFile)
            self._appMgr.HistogramPersistency = "ROOT"
        if debug:
            self._scheduler.OutputLevel = DEBUG
            self._hiveDataBroker.OutputLevel = DEBUG
        # Instantiate the configurable which will set the python
        # logging verbosity at the right time.
        PythonLoggingConf()

    def find_configurable_alg(self, name):
        if not self.configurable_algs:
            raise RuntimeError(
                "not yet configured anything, please call the `configure` method"
            )
        return self._find_impl(name, self.configurable_algs)

    def find_configurable_tool(self, name):
        return self._find_impl(name, self.configurable_tools)

    def _find_impl(self, name, list_to_search):
        for i in list_to_search:
            if name in i.name():
                return i
        raise KeyError('no alg {!r} found'.format(name))

    def register_public_tool(self, tool):
        if not is_tool(tool):
            raise TypeError('{} is not of type Tool'.format(tool))
        self._tools.append(tool)

    def alwaysRunThis(self, alg):
        self._addAlg(alg)
        self._scheduler.AdditionalAlgs += [alg.name]

    def runCallgrind(self, start, stop):
        self.alwaysRunThis(
            Algorithm(
                CallgrindProfile, StartFromEventN=start, StopAtEventN=stop))

    def _addAlg(self, alg):
        if not is_algorithm(alg):
            raise TypeError('{} is not of type Algorithm'.format(alg))
        if alg not in self._algs:
            self._algs.append(alg)

    def addNode(self, node):
        if not _is_node(node):
            raise TypeError('{} is not of type CompositeNode'.format(node))
        if node in self._nodes:
            return
        self._nodes.append(node)
        for c in node.children:
            if _is_node(c):
                self.addNode(c)
            elif is_algorithm(c):
                self._addAlg(c)

    def setup_default_controlflow(self, lines, writer=None):
        dec = CompositeNode(
            'hlt_decision',
            combineLogic=NodeLogic.NONLAZY_OR,
            children=lines,
            forceOrder=False)
        hlt = CompositeNode(
            'HLT',
            combineLogic=NodeLogic.LAZY_AND,
            children=[dec, writer] if writer is not None else [dec],
            forceOrder=True)
        self.addNode(hlt)

    def registerLine(self, name, algs, prescale=1):
        """Add an independent control flow node to the total flow.

        The total application is a NONLAZY_OR of each node added via this
        method.
        """
        if prescale < 1:
            prescaler = Algorithm(
                DeterministicPrescaler,
                properties=dict(AcceptFraction=prescale))
            children = [prescaler] + algs
        else:
            children = algs

        line = CompositeNode(
            name, children, combineLogic=NodeLogic.LAZY_AND, forceOrder=True)
        self._lines.append(line)
        self.addNode(line)

    def plot_data_flow(self, filename='data_flow', extensions=('gv', )):
        """Save a visualisation of the current data flow.

        Parameters
        ----------
        filename : str
            Basename of the file to create.
        extensions : list
            List of file extensions to create. One file is created per
            extensions. Possible values include `'gv'` for saving the raw
            graphviz representation, and `'png'` and `'pdf'` for saving
            graphics.

            **Note**: The `dot` binary must be present on the system for saving
            files with graphical extensions. The raw `.gv` format can be
            convert be hand like::

                dot -Tpdf data_flow.gv > data_flow.pdf
        """
        now = _gaudi_datetime_format(datetime.datetime.now())
        label = 'Data flow generated at {}'.format(now)
        top = pydot.Dot(graph_name='Data flow', label=label, strict=True)
        top.set_node_defaults(shape='box')
        for alg in self._algs:
            alg._graph(top)
        for ext in extensions:
            format = 'raw' if ext == 'gv' else ext
            top.write('{}.{}'.format(filename, ext), format=format)

    def plot_control_flow(self, filename='control_flow', extensions=('gv', )):
        """Save a visualisation of the current control flow.

        Parameters
        ----------
        filename : str
            Basename of the file to create.
        extensions : list
            List of file extensions to create. One file is created per
            extensions. Possible values include `'gv'` for saving the raw
            graphviz representation, and `'png'` and `'pdf'` for saving
            graphics.

            **Note**: The `dot` binary must be present on the system for saving
            files with graphical extensions. The raw `.gv` format can be
            convert be hand like::

                dot -Tpdf data_flow.gv > data_flow.pdf
        """
        # Find the node at the top of the control flow
        # This is the node which is not a child of any other node
        child_nodes = set(
            child for node in self._nodes for child in node.children)
        top_node = None
        for node in self._nodes:
            if node not in child_nodes:
                top_node = node
        if top_node is None:
            raise ConfigurationError('Not not find top control flow node')

        now = _gaudi_datetime_format(datetime.datetime.now())
        label = 'Control flow generated at {}'.format(now)
        graph = pydot.Dot(
            graph_name='control_flow', label=label, strict=True, compound=True)
        top_node._graph(graph)

        for ext in extensions:
            format = 'raw' if ext == 'gv' else ext
            graph.write('{}.{}'.format(filename, ext), format=format)

    def configure(self):
        """Instantiate all underlying Configurables.

        Calling this method is typically the final step of configuration an
        application. The underlying Configurable objects that back the various
        Algorithm and Tool instances used in the control flow are instantiated,
        and they are added to the list of data producers known to the data
        broker. With the control flow previously defined, Gaudi then has all
        the information it needs to schedule all algorithms.
        """
        if self._lines:  # if not, you probably set it up yoself?
            self.setup_default_controlflow(self._lines)

        configuration = dataflow_config()
        for alg in self._algs:
            configuration.update(alg.configuration())
        for tool in self._tools:  # public tools
            configuration.update(tool.configuration())

        # FIXME we make the configurables accessible to be able to hack the ugly things
        odin_loc = '/Event/DAQ/DummyODIN'
        self.configurable_algs, self.configurable_tools = configuration.apply()
        self.configurable_algs += [
            setup_component(
                DummyEventTime,
                "DummyEventTime",
                Start=self._eventClockSvc.InitialTime / 1E9,
                Step=0,
                ODIN=odin_loc,
                IOVLockDep=False),
            setup_component(
                reserveIOV, "reserveIOV", IOVLockDep=False, ODIN=odin_loc)
        ]
        self._hiveDataBroker.DataProducers = self.configurable_algs
        self._scheduler.CompositeCFNodes = [
            node.represent() for node in self._nodes
        ]

        self.plot_data_flow()
        self.plot_control_flow()
