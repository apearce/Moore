###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Wrappers for defining safe yet flexible configuration for Gaudi applications.

PyConf is broadly split in two parts:

1. Wrappers for creating data and control flow using composable Algorithm
   and Tool classes.
2. Helpers for defining a configuration based on function call stacks that
   allow for changes to be configured deep down the stack.

To use algorithms and tools, don't import them from `Configurables` but instead
from `PyConf.Algorithms` and `PyConf.Tools`:

    >>> from PyConf.Algorithms import PrForwardTracking
    >>> from PyConf.Tools import PrAddUTHitsTool

Any single Algorithm or Tool is immutable after instantiation, and hence the dataflow
that produces its inputs is known once the Algorithm or Tool has been created.

The control flow of an application is defined by linking
PyConf.control_flow.CompositeNode instances. Each node has a particular control
flow mode, defined by PyConf.control_flow.NodeLogic.

Application dataflow is automatically deduced by PyConf and Gaudi. The user
only needs to define the _control flow_. For each algorithm in the control
flow, the scheduler ensures that algorithm's data dependencies are met.

Idiomatically, the configuration of individual algorithms is done within small
helper functions that are configured via arguments:

    >>> @configurable
    ... def gec(threshold=9750):
    ...     print('Applying threshold: {}'.format(threshold))

Rather than having to pass changes to default keyword arguments all the way
down a call stack, values can be bound to `@configurable` functions and these
will override the defaults:

    >>> def intermediate_step():
    ...     gec()
    ...
    >>> def run_application():
    ...     with gec.bind(threshold=11000):
    ...         intermediate_step()
    ...
    >>> run_application()
    Applying threshold: 11000

In this example, when the `intermediate_step` function calls `gec`, then value
of the `threshold` that was bound to `gec` inside `run_application`,
1100, will be used instead of the default 9750. The power of this approach is
that the behaviour of `gec` is unaffected outside of the `with` context:

    >>> gec()
    Applying threshold: 9750

Usage of `bind` in HLT1 and HLT2 lines is very rare, as individual lines do not
usually need to configure deep down the call stack, and can instead pass
changes to thresholds directly to the individual functions that create their
input particles.
"""
from PyConf.tonic import configurable, namespace  # noqa
from PyConf.utilities import ConfigurationError  # noqa
