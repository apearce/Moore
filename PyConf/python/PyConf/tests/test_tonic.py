###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import inspect
import re

import pytest

from PyConf.tonic import configurable, bound_parameters, namespace, _bind, debug

# TODO add pre-test func that clears global state in tonic.py?


def test_supported_signatures():
    configurable(lambda: None)
    configurable(lambda a=1: None)
    configurable(lambda **kwargs: None)
    configurable(lambda a=1, **kwargs: None)
    configurable(lambda a: None)
    configurable(lambda a, **kwargs: None)


def test_configurable_wrapping():
    """The configurable wrapper should preserve all arguments."""
    f = lambda a=1, b=2: None
    decorated = configurable(f)

    try:
        f_spec = inspect.getfullargspec(f)
        decorated_spec = inspect.getfullargspec(decorated)
    except AttributeError:
        # Python 2 doesn't have getfullargspec
        f_spec = inspect.getargspec(f)
        decorated_spec = inspect.getargspec(decorated)

    assert f_spec.args == decorated_spec.args
    assert f_spec.keywords == decorated_spec.keywords


def test_bind():
    @configurable
    def func(param='default'):
        return param

    with func.bind(param='override value'):
        assert bound_parameters(func)['param'] == 'override value'

        with func.bind(param='second override'):
            # TODO test for override warning in bind (atm warning is raised only below)
            with pytest.warns(UserWarning):
                assert bound_parameters(func)['param'] == 'second override'

    #positional arguments are namespaces and are not for specifiying arguments
    #FIXME it would be better if this failed, but not sure how since it might be interpreted as namespace
    with func.bind('override value'):
        assert func() == 'default'

    # Falsy namespace selector == no namespace
    with func.bind('', param='override value'):
        pass
    with func.bind():  # test a noop bind
        pass

    with pytest.raises(ValueError):
        with _bind('func', param='override value'):
            pass
    with pytest.raises(ValueError):
        with _bind(hex, param='override value'):
            pass
    with pytest.raises(ValueError):
        with func.bind(nonexistent='override value'):
            pass


def test_bind_within_functions():
    """Bind should work when scoped within functions."""

    @configurable
    def f(a=1):
        return a

    @configurable
    def g():
        with f.bind(a=2):
            return f()

    @configurable
    def h():
        with f.bind(a=3):
            return f()

    assert f() == 1
    assert g() == 2
    # The bind in `g` should not affect f
    assert f() == 1
    assert h() == 3
    assert f() == 1


def test_namespace():
    @configurable
    def func(p1='default', p2='default'):
        pass

    with pytest.warns(UserWarning):
        with func.bind(p1='first'):
            with namespace('mynamespace'):
                with func.bind(p1='second'):
                    assert bound_parameters(func)['p1'] == 'second'
            assert bound_parameters(func)['p1'] == 'first'

        with  func.bind(p2='apple'), \
              func.bind('small', p2='cherry'), \
              func.bind('large', p2='orange'), \
              func.bind('large/really', p2='melon'):
            with namespace('small'):
                assert bound_parameters(func)['p2'] == 'cherry'
            with namespace('large'):
                assert bound_parameters(func)['p2'] == 'orange'
                with namespace('really'):
                    assert bound_parameters(func)['p2'] == 'melon'


def test_bound_parameter_scope():
    @configurable
    def inner(param='default value'):
        assert param == 'second'

    @configurable
    def outer(param='default value'):
        with inner.bind(param='second'):
            inner()

    with inner.bind(param='first'):
        with pytest.warns(UserWarning):
            outer()
        assert bound_parameters(inner)['param'] == 'first'


def test_precedence():
    @configurable
    def func(param='default value'):
        assert param == 'override value'

    with pytest.warns(UserWarning):
        with func.bind(param='override value'):
            func(param='call site value')


def test_positional_arguments():
    @configurable
    def func(param='default value'):
        assert param == 'override value'

    func('override value')
    with pytest.raises(TypeError):  #multiple values for single argument error
        func('override value', param='other value')
    with pytest.raises(TypeError):  #too many arguments error
        func('override value', 'other value')


def test_arguments_without_default():
    @configurable
    def func(param, param2='default value'):
        assert param == 'value'
        assert param2 == 'override value'

    func('value', 'override value')
    func('value', param2='override value')  #positional call
    func(param='value', param2='override value')  #kw call
    with pytest.raises(TypeError):
        func(param2='override value')  #no value was given for param
    with func.bind(param='value'):
        func(param2='override value'
             )  # binding works for functions without defaults
        with pytest.warns(UserWarning):
            func('value', param2='override value')


def _remove_test_method_reference(s):
    """Return string with the reference to the test_debug method removed."""
    # Check that the function name and line number are indeed present
    assert re.match(r'.*test_debug:\d+', s)
    return re.sub(r'test_debug:\d+', 'TEST_MODULE', s)


def test_debug(capsys):
    """Parameter values should be print in the `debug` context."""

    @configurable
    def func(param1='default value', param2=123):
        pass

    func()
    captured = capsys.readouterr()
    assert captured.out == ''

    with debug():
        func()
        captured = capsys.readouterr()
        assert _remove_test_method_reference(captured.out) == '\n'.join([
            'Calling @configurable `func` from TEST_MODULE with non-default parameters: NONE',
        ]) + '\n'

        with func.bind(param1='non-default value'):
            func()
            captured = capsys.readouterr()
            assert _remove_test_method_reference(captured.out) == '\n'.join([
                'Calling @configurable `func` from TEST_MODULE with non-default parameters:',
                '    param1 = non-default value (bound)',
            ]) + '\n'

            func(param2=321)
            captured = capsys.readouterr()
            assert _remove_test_method_reference(captured.out) == '\n'.join([
                'Calling @configurable `func` from TEST_MODULE with non-default parameters:',
                '    param1 = non-default value (bound)',
                '    param2 = 321 (given)'
            ]) + '\n'
