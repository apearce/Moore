###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import re

from PyConf import ConfigurationError
from PyConf.components import Algorithm, Tool
from PyConf.dataflow import DataHandle
from Configurables import (
    Gaudi__Examples__IntDataProducer as IntDataProducer,
    Gaudi__Examples__IntDataConsumer as IntDataConsumer,
    Gaudi__Examples__FloatTool as FloatTool, Gaudi__Examples__THDataConsumer as
    ToolConsumer, Gaudi__Examples__VectorDataProducer as VectorDataProducer,
    Gaudi__Examples__IntVectorsToIntVector as IntVectorsToIntVector)


def test_init():
    # Fail on nonexistent properties
    with pytest.raises(ConfigurationError) as e:
        Algorithm(IntDataProducer, NotAProp=1)
    assert re.search(r'NotAProp.*not propert.* of.*IntDataProducer', str(e))

    # Output property is detected
    producer = Algorithm(IntDataProducer)
    assert len(producer.outputs) == 1
    assert len(producer.inputs) == 0
    data = producer.outputs['OutputLocation']
    assert isinstance(data, DataHandle)
    assert data.producer == producer

    # initializing an algorithm twice should give the same instance
    producer2 = Algorithm(IntDataProducer)
    assert producer is producer2, "algorithms instantiation doesn't seem to be cached correctly"

    with pytest.raises(ConfigurationError):
        # same algorithms shouldn't have different names
        Algorithm(IntDataProducer, name="wurst")

    # Cannot set outputs explicitly
    with pytest.raises(ConfigurationError) as e:
        producer = Algorithm(IntDataProducer, OutputLocation='test')
    assert re.search(r'Cannot set output property OutputLocation explicitly',
                     str(e))

    # Must always provide all inputs
    with pytest.raises(ConfigurationError) as e:
        consumer = Algorithm(IntDataConsumer)
    assert re.match(r'.*provide all inputs.*InputLocation.*', str(e))

    # Type of inputs
    with pytest.raises(TypeError) as e:
        consumer = Algorithm(IntDataConsumer, InputLocation='test')
    assert re.search(r'Inputs.*as DataHandles.*InputLocation.*', str(e))

    consumer = Algorithm(IntDataConsumer, InputLocation=data)
    assert consumer.inputs['InputLocation'] == data

    consumer = Algorithm(IntDataConsumer, InputLocation=producer)
    assert consumer.inputs['InputLocation'] == data

    with pytest.raises(ConfigurationError):
        # cannot change property after instantiation
        consumer.OutputLevel = 2


def test_init_input_location_list():
    """Properties that are lists of DataHandle objects should be supported."""
    multitransformer_bare = Algorithm(IntVectorsToIntVector)
    # FIXME: It is a bug that Algorithm cannot detect an input property for an
    # algorithm that accepts N inputs. This occurs because there's no API in
    # Gaudi for asking if a property is an input, only the heurisitcs of seeing
    # if a default value is a DataHandle. Because the default value here is an
    # empty list, the heuristics fail
    # assert len(multitransformer_bare.inputs) == 1
    assert len(multitransformer_bare.outputs) == 1

    vdp1 = Algorithm(VectorDataProducer)
    vdp2 = Algorithm(VectorDataProducer)
    mt = Algorithm(IntVectorsToIntVector, InputLocations=[vdp1, vdp2])
    mt2 = Algorithm(
        IntVectorsToIntVector,
        InputLocations=[vdp1.OutputLocation, vdp2.OutputLocation])

    # If we initialize the MultiTransformer with inputs, then we can detect the
    # DataHandles and everything looks sensible (wrt. the FIXME above)
    assert len(mt.inputs) == 1
    assert len(mt2.inputs) == 1
    assert len(mt.inputs['InputLocations']) == 2
    assert len(mt2.inputs['InputLocations']) == 2
    assert all(isinstance(x, DataHandle) for x in mt.inputs['InputLocations'])
    assert all(isinstance(x, DataHandle) for x in mt2.inputs['InputLocations'])


def test_tool_compat():
    producer = Algorithm(IntDataProducer)
    alg = Algorithm(
        ToolConsumer,
        FloatTool=Tool(FloatTool, Input=producer),
        InputLocation=producer)
    assert len(alg.tools) == 1

    with pytest.raises(TypeError):
        Algorithm(
            ToolConsumer, FloatTool=FloatTool(),
            InputLocation=producer)  # don't be a fool, wrap your tool

    # A Tool instance should be usable in multiple (different) Algorithms
    tool = Tool(FloatTool, Input=producer)
    alg1 = Algorithm(ToolConsumer, FloatTool=tool, InputLocation=producer)
    alg2 = Algorithm(
        ToolConsumer, FloatTool=tool, InputLocation=producer,
        OutputLevel=0)  # make the second alg slightly different
    assert alg1.tools == alg2.tools


def algorithm_equal_hashes():
    """Two Algorithms should have equal hashes if their properties and inputs are equal."""
    pass


def algorithm_unequal_hashes():
    """Two Algorithms should not have equal hashes if their properties and inputs are not equal."""
    pass


def algorithm_hash_forced_outputs():
    """An Algorithm with a forced output should have a different hash to one without."""
    pass
