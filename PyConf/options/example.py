###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""An example application configured using PyConf components.

This illustrates:

* The usage of @configurable and bind to override default keyword argument
  values within a call stack
* How to define control flow using CompositeNode
"""
from PyConf import configurable
from PyConf.environment import EverythingHandler
from PyConf.control_flow import CompositeNode
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as IntDataProducer
from PyConf.Algorithms import Gaudi__Examples__IntIntToFloatFloatData as IntIntToFloatFloatData
from Gaudi.Configuration import DEBUG


@configurable
def int_maker(value=1):
    # The IntDataProducer isn't actually configurable, but we accept an
    # argument in this method to illustrate @configurable and bind
    print('Got value {}'.format(value))
    return IntDataProducer().OutputLocation


def make_first_float():
    int1 = int_maker()
    with IntDataProducer.bind(
            OutputLevel=DEBUG):  # change some property of IntDataProducer
        int2 = int_maker()
    return IntIntToFloatFloatData(
        InputLocation1=int1, InputLocation2=int2).OutputLocation1


def make_second_float():
    int1 = int_maker()
    int2 = int_maker()
    return IntIntToFloatFloatData(
        InputLocation1=int1, InputLocation2=int2).OutputLocation2


# Bind to the @configurable int_maker to override the default value when it's
# called
with int_maker.bind(value=123):
    float1 = make_first_float()
# We're out of the scope of the `bind`, so expect to see the default value here
float2 = make_second_float()

# Note that we don't we to explicitly 'schedule' IntDataProducer here; the fact
# that we've instantiated it and used it as a data dependency of float1 and
# float2 is enough for the framework to know IntDataProducer needs to be run
line1 = CompositeNode("line1", children=[float1, float2])

# Define the application environment and run it
e = EverythingHandler(threadPoolSize=4, nEventSlots=5, evtMax=50)
e.addNode(line1)
e.configure()
